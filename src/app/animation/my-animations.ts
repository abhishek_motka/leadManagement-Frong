import {
   trigger,
	state,
	style,
	transition,
	animate,
	keyframes
} from '@angular/core';

export const FocusAnimation= trigger(`FocusPanel`,[
	state('inactive', style({
		transform:'scale(0.5)'
	})),
	state('active',style({
		transform:	'scale(1.0)'
	})),
	transition('inactive => active', animate('100ms ease-in')),
	transition('active => inactive', animate('100ms ease-out'))
]);
export const MoveAnimation = trigger(`MovePanel`,[
	transition('void => *', [
		animate(450, keyframes([
			style({opacity: 0, transform:'translateX(100px)', offset:0}),
			style({opacity: 0.4, transform:'translateX(25px)', offset:.75}),
			style({opacity: 1, transform:'translateX(0)', offset:1})
		]))
	])
]);
export const MoveUpAnimation = trigger(`MoveUpPanel`,[
	transition('void => *', [
		animate(450, keyframes([
			style({opacity: 0, transform:'translateY(100px)', offset:0}),
			style({opacity: 0.4, transform:'translateY(25px)', offset:.75}),
			style({opacity: 1, transform:'translateY(0)', offset:1})
		]))
	])
]);
