import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LocationStrategy, HashLocationStrategy} from '@angular/common';

//Import all modules
import { AppRoutingModule } from './app-routing.module';
import { DashboardModule } from './components/dashboard/dashboard.module';

//Import all components for this module
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';

//Import services which are accessible in whole application
import { UserService } from './services/user.service';
import { OnlineService } from './components/dashboard/services/online.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { PasswordResetService } from './services/password-reset.service';
import { AuthGuard } from './guards/auth-guard.service';
import { AdminGuard } from './guards/admin-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    LoginComponent,
    PageNotFoundComponent,
    ForgetPasswordComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
	  FlashMessagesModule,
    DashboardModule,
	  AppRoutingModule
  ],
  providers: [
    FlashMessagesService,
    PasswordResetService,
    UserService,
    AuthGuard,
    AdminGuard,
    OnlineService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
