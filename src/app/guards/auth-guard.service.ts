//guard to protect routes from accessing without login
import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild{

  constructor(
    private router: Router,
    private userService: UserService,
    private flashMessage: FlashMessagesService
  ) { }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> {
    if(this.userService.isTokenValid()){
       return this.userService.isLoggedIn();
    }else{
      this.router.navigate(['/login']);
      return Observable.of(false);
    }
  }

  canActivateChild( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> | boolean {
    return this.canActivate(route, state);
  }
}
