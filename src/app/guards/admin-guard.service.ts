//guard to protect routes accessible to admins only
import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of';

@Injectable()
export class AdminGuard implements CanActivate, CanActivateChild{

  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> | boolean {
    var user = this.userService.loadUserData();
    if(!user || !user.type){
        return false;
    }
    if(user.type === 'admin'){
       return true;
    }else{
      this.router.navigate(['/dashboard']);
      return false;
    }
  }

  canActivateChild( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> | boolean {
    return this.canActivate(route, state);
  }
}
