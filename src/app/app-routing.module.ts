//Routing module for root components

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

//Routes that are used in root components
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'forgotpassword/send', component: ForgetPasswordComponent, data: {send: true} },
  { path: 'forgotpassword/reset', component: ForgetPasswordComponent, data: {send: false} },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' }, //By default redirect to dashboard
  { path: '**', component: PageNotFoundComponent } //If no path match go to 404
];

//Export router module so that other modules can use the directives offered by RoutingModule
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
