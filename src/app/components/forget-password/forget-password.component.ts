//this component is displayed when clicked on forgot password link
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { PasswordResetService } from '../../services/password-reset.service';
import { NgForm } from '@angular/forms';
import { FocusAnimation, MoveAnimation } from '../../animation/my-animations';


@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css'],
  animations:[
	  		FocusAnimation,
			MoveAnimation
		 ]
})
export class ForgetPasswordComponent implements OnInit {
  //data bound with send mail form
  state: string = 'active';
  user = {
    email: ""
  };

  //when true show send link form otherwise display reset password form
  send = true;

  //data bound with reset password form
  resetPassword = {
    newPassword: "",
    confirmPassword: ""
  };

  //to store hash sent with link in email
  hash = '';

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private resetService: PasswordResetService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    //access data field sent with the route
    this.route.data.subscribe(data => {
      if(data.send !== undefined)
        this.send = data.send;

      //if path contains check parameter then save it to hash variable
      this.route.queryParams.subscribe(params => {
        if(params['check'])
          this.hash = params['check'];
        else if(!this.send)
          this.flashMessage.show('You can\'t use this link to reset password.', {cssClass: 'alert-danger', timeout: 1500});
      });
    });
  }

  //navigate to login page
  onBackClick(): void {
    this.router.navigate(['/login']);
  }

  //on send email form submitted
  onFormSubmit(form: NgForm): void {
    //use reset password service to send resetPassword link
    this.resetService.sendResetEmail(this.user).subscribe(data => {

      if(data.success) {

        this.flashMessage.show("A Password Reset linke is sent to your email address.", {cssClass: 'alert-success', timeout: 1500});
        this.user.email = "";

        var elements = document.getElementsByTagName('input');
        for(var i = 0; i < elements.length; i++)
          elements.item(i).blur();
        form.reset();

      }else{

        //if error, show error
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in sending email with password reset link.");

        var elements = document.getElementsByTagName('input');
        for(var i = 0; i < elements.length; i++)
          elements.item(i).blur();
        form.reset();
      }
    });
  }

  //function executes when user click reset password button
  onResetFormSubmit(form: NgForm): void {
      var object = {
        hash: this.hash,
        password: this.resetPassword.newPassword
      }

      //use hash provided in the route link to verify and reset password
      this.resetService.resetPassword(object).subscribe(response => {
        if(response.success){
          this.flashMessage.show("You password is reset.", {cssClass: 'alert-success', timeout: 1500});
          this.router.navigate(['/login']);
        }else{
          this.flashMessage.show("Error in resetting your password.", {cssClass: 'alert-danger', timeout: 1500});
        }
      });
  }
}
