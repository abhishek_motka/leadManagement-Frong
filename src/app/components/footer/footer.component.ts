//footer for the site
import { Component } from '@angular/core';
import { Config } from '../../app.config';

@Component({
  selector: 'my-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})

export class FooterComponent {
  copyright = Config.COPYRIGHT;
}
