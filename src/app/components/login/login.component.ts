import { Component } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../services/user.service';
import { OnlineService } from '../dashboard/services/online.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FocusAnimation, MoveAnimation } from '../../animation/my-animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations:[
	  		FocusAnimation,
			  MoveAnimation
		 ]
})
export class LoginComponent{

  //Data bound with html template
  state: string = 'active';

  loginCredentials = {
		username: "",
		password: ""
	};

  constructor(
      private flashMessage: FlashMessagesService,
      private userService: UserService,
      private router: Router,
      private route: ActivatedRoute,
      private onlineService: OnlineService
  ) {}

  //function executes when user clicks login button
	onLoginSubmit(loginForm: any): void {

    //use userservice to authenticate user
		this.userService.authenticate(this.loginCredentials).subscribe(data => {

      //if success field of returned data is true then user is authenticate
      if(data.success){

        //use returned user data and token to local storage
				this.userService.saveUserData(data.user, data.token);
				this.flashMessage.show('You are logged in.', {cssClass: 'alert-success alert-dismissible', timeout: 1500});

        //after authenticate navigate to dashboard of the user
				this.router.navigate(['/dashboard']);
			}else{
        //show error message
        this.flashMessage.show(data.msg, {cssClass: 'alert-danger alert-dismissible', timeout: 1500});

        //remove focus from all form controls and reset form to prestine state
        var elements = document.getElementsByTagName('input');
        for(var i = 0; i < elements.length; i++)
          elements.item(i).blur();
        loginForm.reset();
			}
		});
	}

  //when user click on forgot password link navigate to forgot password page
  onForgetPassClick(): void {
    this.router.navigate(['/forgotpassword/send']);
  }

}
