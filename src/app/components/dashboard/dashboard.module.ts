//module for all main features of the application
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ChartsModule } from 'ng2-charts';
import { DatepickerModule } from 'angular2-material-datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import * as $ from 'jquery';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';

import { DashboardRoutingModule } from './dashboard-routing.module';

import { UserService } from '../../services/user.service';
import { StatusService } from './services/master/status.service';
import { SourceService } from './services/master/source.service';
import { RequirementService } from './services/master/requirement.service';
import { RoleService } from './services/master/role.service';
import { LeadService } from './services/lead.service';
import { FollowupsService } from './services/followups.service';

import { DashboardComponent } from '../dashboard/dashboard.component';
import { SourceComponent } from './components/master/source/source.component';
import { RequirementComponent } from './components/master/requirement/requirement.component';
import { RoleComponent } from './components/master/role/role.component';
import { ProfileBarComponent } from './components/profile-bar/profile-bar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { StatusComponent } from './components/master/status/status.component';
import { UserMngmntComponent } from './components/user-mngmnt/user-mngmnt.component';
import { LeadComponent } from './components/lead/lead.component';
import { ReportComponent } from './components/report/report.component';
import { ConsoleComponent } from './components/console/console.component';

import { StatusfilterPipe } from '../../pipe/statusfilter.pipe';
import { LeadfilterPipe } from '../../pipe/leadfilter.pipe';
import { UserFilterPipe } from '../../pipe/user-filter.pipe';
import { MasterFilterPipe } from '../../pipe/master-filter.pipe';
import { OrderByPipe } from '../../pipe/order-by.pipe';

import { Config } from '../../app.config';

const config: SocketIoConfig = {url: Config.SOCKET_SERVER_URL, options: {}};

@NgModule({
  declarations: [
    DashboardComponent,
    ProfileBarComponent,
    NavbarComponent,
    ChangePasswordComponent,
    StatusComponent,
    SourceComponent,
    RequirementComponent,
    RoleComponent,
    UserMngmntComponent,
    LeadComponent,
    StatusfilterPipe,
    LeadfilterPipe,
    ReportComponent,
    UserFilterPipe,
    MasterFilterPipe,
    OrderByPipe,
    ConsoleComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
	  FlashMessagesModule,
    DashboardRoutingModule,
    ChartsModule,
    DatepickerModule,
    DatepickerModule,
    NgxPaginationModule,
    NKDatetimeModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [
    FlashMessagesService,
    UserService,
    StatusService,
    SourceService,
    RequirementService,
    RoleService,
    LeadService,
    FollowupsService
  ]
})

export class DashboardModule { }
