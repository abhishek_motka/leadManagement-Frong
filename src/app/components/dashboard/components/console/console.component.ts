import { Component, OnInit, DoCheck } from '@angular/core';
import { LeadService } from '../../services/lead.service';
import { StatusService } from '../../services/master/status.service';
import { UserService } from '../../../../services/user.service';
import { OnlineService } from '../../services/online.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DatePipe } from '@angular/common';
import { FocusAnimation, MoveAnimation, MoveUpAnimation } from '../../../../animation/my-animations';
import { Config } from '../../../../app.config';

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.css'],
  animations:[
 		  FocusAnimation,
 		  MoveAnimation,
		  MoveUpAnimation
 		]
})

export class ConsoleComponent implements OnInit {
  state: string = 'active';
  leads = [];
  todayLeads = 0;
  upcomingLeads = 0;
  overdueLeads = 0;
  successLeads = 0;
  barchartData = [];
  statusWise = {}
  dateWise = [{}, {}, {}, {}, {}, {}, {}];
  weekWise = [{}, {}, {}, {}];
  monthWise = [{}, {}, {}];
  userWise = {};
  sourceWise = {};
  totalLeadsWeek = 0;
  totalLeadsMonth = 0;
  totalLeadsQuater = 0;
  statusDisplay = "date";
  userDisplay = "week";
  keysGetter = Object.keys;
  datePipe = new DatePipe('en-In');
  colors = Config.CHART_COLORS;

  //to keep track of online users
  onlineUsers = [];

  //data for doughnut chart
  public sourceChartLabels: string[] = [];
  public sourceChartData: number[] = [];
  public sourceChartType: string = 'doughnut';
  sourceChartColors = [];


  //data for bar chart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  }
  barChartType = 'bar';
  barChartLegend = true;
  public barChartData:any[] = [
    {label: "OPEN", data: [0,0,0,0,0,0,0]},
    {label: "WORKING", data: [0,0,0,0,0,0,0]},
    {label: "SUCCESS", data: [0,0,0,0,0,0,0]},
    {label: "FAIL", data: [0,0,0,0,0,0,0]},
  ];

  //set labels of bar chart
  todayDate = new Date();
  public barChartLabels = [
    this.datePipe.transform(this.todayDate.setDate(this.todayDate.getDate()-7), 'dd-MM-yyyy'),
    this.datePipe.transform(this.todayDate.setDate(this.todayDate.getDate()+1), 'dd-MM-yyyy'),
    this.datePipe.transform(this.todayDate.setDate(this.todayDate.getDate()+1), 'dd-MM-yyyy'),
    this.datePipe.transform(this.todayDate.setDate(this.todayDate.getDate()+1), 'dd-MM-yyyy'),
    this.datePipe.transform(this.todayDate.setDate(this.todayDate.getDate()+1), 'dd-MM-yyyy'),
    this.datePipe.transform(this.todayDate.setDate(this.todayDate.getDate()+1), 'dd-MM-yyyy'),
    this.datePipe.transform(this.todayDate.setDate(this.todayDate.getDate()+1), 'dd-MM-yyyy'),
  ]

  constructor(
    private leadService: LeadService,
    private flashMessage: FlashMessagesService,
    private statusService: StatusService,
    private userService: UserService,
    private onlineService: OnlineService
  ) { }

  ngOnInit() {

    //retriev all lead details
    this.leadService.loadLeads().subscribe(data => {
      if(data.success){
        this.leads = data.leads;

        //extract required details from all leads
        for(var i = 0; i < this.leads.length; i++){
          var lead = this.leads[i];

          //create an object which contains number of leads sourcewise
          if(lead.source.toUpperCase() in this.sourceWise){
            this.sourceWise[lead.source.toUpperCase()]++;
          }else{
            this.sourceWise[lead.source.toUpperCase()] = 1;
          }

          //get details about today followup, upcoming follow up and overdues by comparing next follow up date with current date and next month date
          var leadDate = new Date(Date.parse(lead.nextDate));
          var today = new Date();
          switch(this.compareDates(leadDate, today)){
            case 1:
              today.setDate(today.getDate()+30);
              if(this.compareDates(leadDate, today) <= 0){
                this.upcomingLeads++;
              }
              break;
            case 0:
              this.todayLeads++;
              break;
            case -1:
              this.overdueLeads++;
              break;
          }

          //get number of successful lead this month
          if(lead.status.toUpperCase() === 'SUCCESS'){
            today = new Date();
            if(today.getMonth() === leadDate.getMonth() && today.getFullYear() === leadDate.getFullYear())
              this.successLeads++;
          }

          today = new Date();
          today.setDate(today.getDate()-7);
          today.setHours(0);
          today.setMinutes(0);

          //check whether follow up is in last week, (for weekly analysis)
          if(this.compareDates(leadDate, today) >= 0 && this.compareDates(leadDate, new Date()) < 0){
            //add to total leads for week
            this.totalLeadsWeek++;

            //if follow up is in previous week, increment no. of leads of lead's status in statuswise
            if(lead.status.toUpperCase() in this.statusWise)
              this.statusWise[lead.status.toUpperCase()]++;
            else
              this.statusWise[lead.status.toUpperCase()] = 1;

            //add or icrement no. of leads in days wise lead status for bar chart graph
            if(lead.status.toUpperCase() in this.dateWise[leadDate.getDate() - today.getDate()])
              (this.dateWise[leadDate.getDate() - today.getDate()])[lead.status.toUpperCase()]++;
            else
              (this.dateWise[leadDate.getDate() - today.getDate()])[lead.status.toUpperCase()] = 1;

            //extract details of leads userwise for user analysis
            //check if user of lead exists in userwise object or not
            if(lead.userid in this.userWise){
              var user = this.userWise[lead.userid];

              //if status type exist in user then increment otherwise add
              if(lead.status.toUpperCase() in user){
                user[lead.status.toUpperCase()]++;
                user['assigned']++;
              }else{
                user[lead.status.toUpperCase()] = 1;
                user['assigned']++;
              }

            }else{
              //if user doesn't exist then add new user in userwise object and add status type in user object
              this.userWise[lead.userid] = {};
              var user = this.userWise[lead.userid];
              user[lead.status.toUpperCase()] = 1;
              user['name'] = lead.assignment;
              user['assigned'] = 1;
            }
          }
        }

        //userwise will have data of those users who are assigned lead
        //get details of other users and initialize user object with all status type to 0
        if(this.userService.loadUserData().type === 'admin') {

          //if type is admin get details of all user
          this.userService.getAllUsers().subscribe(data => {

            if(data.success){
              var users = data.users;

              for(var i = 0; i < users.length; i++){
                if(!(users[i]._id in this.userWise)){
                  this.userWise[users[i]._id] = {};
                  var user = this.userWise[users[i]._id];
                  user.OPEN = 0;
                  user.WORKING = 0;
                  user.SUCCESS = 0;
                  user.FAIL = 0;
                  user.name = users[i].firstName+" "+users[i].lastName;
                  user.assigned = 0;
                }
              }

            }

          });
        }else{

          //otherwise get details of logged in user
          var user = this.userService.loadUserData();
          if(!(user._id in this.userWise)){
            this.userWise[user._id] = {};
            this.userWise[user._id].OPEN = 0;
            this.userWise[user._id].SUCCESS = 0;
            this.userWise[user._id].WORKING = 0;
            this.userWise[user._id].FAIL = 0;
            this.userWise[user._id].name = user.name;
            this.userWise[user._id].assigned = 0;
          }

        }

        //update source chart data using sourceWise object
        var index = 3;
        for(var key in this.sourceWise){
          this.sourceChartLabels.push(key);
          this.sourceChartData.push(this.sourceWise[key]);
          this.sourceChartColors.push(this.colors[index++%10]);
        }


        //update bar chart data using datewise object for weekly analysis
        for(var key in this.statusWise){
          var barData = {
            data: [],
            label: key
          };
          for(var j = 0; j < this.dateWise.length; j++){
            if(key.toUpperCase() in this.dateWise[j])
              barData.data.push((this.dateWise[j])[key]);
            else
              barData.data.push(0);
          }

          //categorize data in 4 category based on status
          switch(key.toUpperCase()){
            case 'OPEN':
              this.barChartData[0] = barData;
              break;
            case 'SUCCESS':
              this.barChartData[2] = barData;
              break;
            case 'FAIL':
              this.barChartData[3] = barData;
              break;
            default:
              for(var k = 0; k < barData.data.length; k++){
                this.barChartData[1].data[k] += barData.data[k];
              }
              break;
          }
        }

        //add remaining statuses to statusWise and initialize them with 0
        this.statusService.loadStatuses().subscribe(data => {
          if(data.success){
            var statuses = data.status;
            for(var i = 0; i < statuses.length; i++){
              if(!(statuses[i].name.toUpperCase() in this.statusWise)){
                this.statusWise[statuses[i].name.toUpperCase()] = 0;
                /*
                var barData = {
                  data: [],
                  label: key
                };
                for(var j = 0; j < this.dateWise.length; j++){
                  if(statuses[i].name.toUpperCase() in this.dateWise[j])
                    barData.data.push((this.dateWise[j])[key]);
                  else
                    barData.data.push(0);
                }
                switch(statuses[i].name.toUpperCase()){
                  case 'OPEN':
                    this.barChartData[0] = barData;
                    break;
                  case 'SUCCESS':
                    this.barChartData[2] = barData;
                    break;
                  case 'FAIL':
                    this.barChartData[3] = barData;
                    break;
                  default:
                    for(var k = 0; k < barData.data.length; k++){
                      this.barChartData[1].data[k] += barData.data[k];
                    }
                    break;
                }
                */
              }
            }
            //force redraw chart
            this.barChartData = JSON.parse(JSON.stringify(this.barChartData));
          }
        });
      }else{
        this.flashMessage.show("Error in retrieving details.", {cssClass: 'alert-danger', timeout: 1500});
        this.leads = [];
      }
    });

    //get details of online users using online service
    this.onlineService.getOnlineUsers().subscribe(data => {
      if(data.success)
        this.onlineUsers = data.users;
    });

    //listen to server socket for addition of new online user
    this.onlineService.getSocket().on('broadcast', (data) => {
      if(data.user){

        //if new user connected then add it to online user array after parsing JSON object
        var user = JSON.parse(data.user);
        var exists = false;

        //check if user already exists, (this happens when same user logs in from different browser or device)
        for(var i = 0; i < this.onlineUsers.length; i++){
          if(this.onlineUsers[i].username === user.username)
            exists = true;
        }

        //if doesn't exist then add the user
        if(!exists)
          this.onlineUsers.push(user);
      }
    });

    //listen to socket server for user being offline
    this.onlineService.getSocket().on('remove', (data) => {

      if(data.user){
        //remove user from local onlineUsers array by creating new array
        var user = JSON.parse(data.user);
        var newSet = [];
        for(var i = 0; i < this.onlineUsers.length; i++){
          if(this.onlineUsers[i].username !== user.username)
            newSet.push(this.onlineUsers[i]);
        }
        this.onlineUsers = JSON.parse(JSON.stringify(newSet));
      }
    });
  }

  //when user clicks on monthly status analysis button
  onMonthlyStatusClick(): void {
    //reset statusWise to empty object and set display to week
    this.statusDisplay = "week";
    this.statusWise = {};

    //set bar chart labels
    var today = new Date();
    today.setDate(today.getDate()-28);
    this.barChartLabels= [
      this.datePipe.transform(today, 'dd-MM')+"/"+this.datePipe.transform(today.setDate(today.getDate()+6), 'dd-MM'),
      this.datePipe.transform(today.setDate(today.getDate()+1), 'dd-MM')+"/"+this.datePipe.transform(today.setDate(today.getDate()+6), 'dd-MM'),
      this.datePipe.transform(today.setDate(today.getDate()+1), 'dd-MM')+"/"+this.datePipe.transform(today.setDate(today.getDate()+6), 'dd-MM'),
      this.datePipe.transform(today.setDate(today.getDate()+1), 'dd-MM')+"/"+this.datePipe.transform(today.setDate(today.getDate()+6), 'dd-MM')
    ];

    //reset bar chart data
    this.barChartData[0].data = [0,0,0,0];
    this.barChartData[1].data = [0,0,0,0];
    this.barChartData[2].data = [0,0,0,0];
    this.barChartData[3].data = [0,0,0,0];

    //reset weekwise data
    this.weekWise = [{}, {}, {}, {}];
    this.totalLeadsMonth = 0;

    //iterate through all leads to extract status analysis
    for(var i = 0; i < this.leads.length; i++){
      var lead = this.leads[i];

      var leadDate = new Date(Date.parse(lead.nextDate));
      var today = new Date();
      today.setDate(today.getDate()-28);
      today.setHours(0);
      today.setMinutes(0);

      //compare leadDate to check whether it is in range of one month or not
      if(this.compareDates(leadDate, today) >= 0 && this.compareDates(leadDate, new Date()) < 0){

        //increment total leads for monthwise
        this.totalLeadsMonth++;

        //if status doesn't exist in statuswise then add otherwise increment
        if(lead.status.toUpperCase() in this.statusWise)
          this.statusWise[lead.status.toUpperCase()]++;
        else
          this.statusWise[lead.status.toUpperCase()] = 1;

        //compute difference of leadDate and start date
        var diffDays = Math.floor(Math.abs(leadDate.getTime() - today.getTime()) / (1000 * 3600 * 24));
        var weekDay = Math.floor(diffDays/7);

        //add status details to weekWise array with given week no
        if(lead.status.toUpperCase() in this.weekWise[weekDay])
          (this.weekWise[weekDay])[lead.status.toUpperCase()]++;
        else
          (this.weekWise[weekDay])[lead.status.toUpperCase()] = 1;
      }
    }

    //update bar chart data
    for(var key in this.statusWise){
      var barData = {
        data: [],
        label: key
      };

      for(var j = 0; j < this.weekWise.length; j++){
        if(key.toUpperCase() in this.weekWise[j])
          barData.data.push((this.weekWise[j])[key]);
        else
          barData.data.push(0);
      }

      //categorize data by status
      switch(key.toUpperCase()){
        case 'OPEN':
          this.barChartData[0] = barData;
          break;
        case 'SUCCESS':
          this.barChartData[2] = barData;
          break;
        case 'FAIL':
          this.barChartData[3] = barData;
          break;
        default:
          for(var k = 0; k < barData.data.length; k++){
            this.barChartData[1].data[k] += barData.data[k];
          }
          break;
      }
    }

    //add remaining statuses to statuswise object
    this.statusService.loadStatuses().subscribe(data => {
      if(data.success){

        var statuses = data.status;

        for(var i = 0; i < statuses.length; i++){

          if(!(statuses[i].name.toUpperCase() in this.statusWise)){
            this.statusWise[statuses[i].name.toUpperCase()] = 0;
            /*
            var barData = {
              data: [],
              label: key
            };
            for(var j = 0; j < this.weekWise.length; j++){
              if(statuses[i].name.toUpperCase() in this.weekWise[j])
                barData.data.push((this.weekWise[j])[key]);
              else
                barData.data.push(0);
            }
            switch(statuses[i].name.toUpperCase()){
              case 'OPEN':
                this.barChartData[0] = barData;
                break;
              case 'SUCCESS':
                this.barChartData[2] = barData;
                break;
              case 'FAIL':
                this.barChartData[3] = barData;
                break;
              default:
                for(var k = 0; k < barData.data.length; k++){
                  this.barChartData[1].data[k] += barData.data[k];
                }
                break;
            }
            */
          }
        }
        this.barChartData = JSON.parse(JSON.stringify(this.barChartData));
      }
    });
  }

  //when user clicks on weekly status analysis buttton
  onWeeklyStatusClick(): void {

    //reset statusWise and statusDisplay variables
    this.statusDisplay = "date";
    this.statusWise = {};

    //set bar chart labels
    var today = new Date();
    today.setDate(today.getDate()-7);
    this.barChartLabels= [
      this.datePipe.transform(today, 'dd-MM-yyyy'),
      this.datePipe.transform(today.setDate(today.getDate()+1), 'dd-MM-yyyy'),
      this.datePipe.transform(today.setDate(today.getDate()+1), 'dd-MM-yyyy'),
      this.datePipe.transform(today.setDate(today.getDate()+1), 'dd-MM-yyyy'),
      this.datePipe.transform(today.setDate(today.getDate()+1), 'dd-MM-yyyy'),
      this.datePipe.transform(today.setDate(today.getDate()+1), 'dd-MM-yyyy'),
      this.datePipe.transform(today.setDate(today.getDate()+1), 'dd-MM-yyyy'),
    ];

    //reset bar chart data
    this.barChartData[0].data = [0,0,0,0,0,0,0];
    this.barChartData[1].data = [0,0,0,0,0,0,0];
    this.barChartData[2].data = [0,0,0,0,0,0,0];
    this.barChartData[3].data = [0,0,0,0,0,0,0];

    //reset dateWise data
    this.dateWise = [{},{},{},{},{},{},{}];
    this.totalLeadsWeek = 0;

    //iterate through all leads
    for(var i = 0; i < this.leads.length; i++){
      var lead = this.leads[i];
      var leadDate = new Date(Date.parse(lead.nextDate));

      var today = new Date();
      today.setDate(today.getDate()-7);
      today.setHours(0);
      today.setMinutes(0);

      //compare leadDate to check if it is in the range of previous week
      if(this.compareDates(leadDate, today) >= 0 && this.compareDates(leadDate, new Date()) < 0){

        //increment total leads for the last week
        this.totalLeadsWeek++;

        //add or increment status of statusWise object
        if(lead.status.toUpperCase() in this.statusWise)
          this.statusWise[lead.status.toUpperCase()]++;
        else
          this.statusWise[lead.status.toUpperCase()] = 1;

        //create dateWise data using leads
        if(lead.status.toUpperCase() in this.dateWise[leadDate.getDate() - today.getDate()])
          (this.dateWise[leadDate.getDate() - today.getDate()])[lead.status.toUpperCase()]++;
        else
          (this.dateWise[leadDate.getDate() - today.getDate()])[lead.status.toUpperCase()] = 1;
      }

    }

    //update bar chart data
    for(var key in this.statusWise){
      var barData = {
        data: [],
        label: key
      };

      for(var j = 0; j < this.dateWise.length; j++){
        if(key.toUpperCase() in this.dateWise[j])
          barData.data.push((this.dateWise[j])[key]);
        else
          barData.data.push(0);
      }

      //categorize leads in statuses
      switch(key.toUpperCase()){
        case 'OPEN':
          this.barChartData[0] = barData;
          break;
        case 'SUCCESS':
          this.barChartData[2] = barData;
          break;
        case 'FAIL':
          this.barChartData[3] = barData;
          break;
        default:
          for(var k = 0; k < barData.data.length; k++){
            this.barChartData[1].data[k] += barData.data[k];
          }
          break;
      }

    }

    //add remaining statuses in statusWise object
    this.statusService.loadStatuses().subscribe(data => {

      if(data.success){

        var statuses = data.status;

        for(var i = 0; i < statuses.length; i++){
          if(!(statuses[i].name.toUpperCase() in this.statusWise)){
            this.statusWise[statuses[i].name.toUpperCase()] = 0;
            /*
            var barData = {
              data: [],
              label: key
            };
            for(var j = 0; j < this.dateWise.length; j++){
              if(statuses[i].name.toUpperCase() in this.dateWise[j])
                barData.data.push((this.dateWise[j])[key]);
              else
                barData.data.push(0);
            }
            switch(statuses[i].name.toUpperCase()){
              case 'OPEN':
                this.barChartData[0] = barData;
                break;
              case 'SUCCESS':
                this.barChartData[2] = barData;
                break;
              case 'FAIL':
                this.barChartData[3] = barData;
                break;
              default:
                for(var k = 0; k < barData.data.length; k++){
                  this.barChartData[1].data[k] += barData.data[k];
                }
                break;
            }*/
          }
        }

        //force redraw bar chart
        this.barChartData = JSON.parse(JSON.stringify(this.barChartData));
      }
    });
  }

  //when user clicks on quaterly button in statuswise analysis
  onQuaterlyStatusClick(): void {

    //reset statusDisplay and statusWise variables
    this.statusDisplay = "month";
    this.statusWise = {};

    //set bar chart labels
    var today = new Date();
    var quarter = Math.floor(today.getMonth()/3);
    var year = today.getFullYear();
    this.barChartLabels= [
      this.datePipe.transform(new Date(year, quarter*3, 1, 0, 0, 0, 0), 'dd-MM-yyyy')+"/"+this.datePipe.transform(new Date(year, quarter*3, 30, 0, 0, 0, 0), 'dd-MM-yyyy'),
      this.datePipe.transform(new Date(year, quarter*3+1, 1, 0, 0, 0, 0), 'dd-MM-yyyy')+"/"+this.datePipe.transform(new Date(year, quarter*3+1, 30, 0, 0, 0, 0), 'dd-MM-yyyy'),
      this.datePipe.transform(new Date(year, quarter*3+2, 1, 0, 0, 0, 0), 'dd-MM-yyyy')+"/"+this.datePipe.transform(new Date(year, quarter*3+2, 30, 0, 0, 0, 0), 'dd-MM-yyyy')
    ];

    //reset bar chart data
    this.barChartData[0].data = [0,0,0];
    this.barChartData[1].data = [0,0,0];
    this.barChartData[2].data = [0,0,0];
    this.barChartData[3].data = [0,0,0];

    //reset monthWise data
    this.monthWise = [{},{},{}];
    this.totalLeadsQuater = 0;

    //iterate through all leads to extract status analysis data for this quarter
    for(var i = 0; i < this.leads.length; i++){
      var lead = this.leads[i];

      //get details of current quarter
      var leadDate = new Date(Date.parse(lead.nextDate));
      var today = new Date(year, quarter*3, 1, 0, 0, 0, 0);
      var endDate = new Date(year, quarter*3+2, 30, 0, 0, 0, 0);

      //check if the lead has followup in current quarter
      if(this.compareDates(leadDate, today) >= 0 && this.compareDates(leadDate, endDate) < 0){

        //increment total leads of current quarter
        this.totalLeadsQuater++;

        //add or increment status details in statusWise object
        if(lead.status.toUpperCase() in this.statusWise)
          this.statusWise[lead.status.toUpperCase()]++;
        else
          this.statusWise[lead.status.toUpperCase()] = 1;

        //create monthWise data to use for bar chart
        var month = leadDate.getMonth() - quarter*3;
        if(lead.status.toUpperCase() in this.monthWise[month])
          (this.monthWise[month])[lead.status.toUpperCase()]++;
        else
          (this.monthWise[month])[lead.status.toUpperCase()] = 1;
      }
    }

    //update bar chart data
    for(var key in this.statusWise){

      var barData = {
        data: [],
        label: key
      };

      for(var j = 0; j < this.monthWise.length; j++){
        if(key.toUpperCase() in this.monthWise[j])
          barData.data.push((this.monthWise[j])[key]);
        else
          barData.data.push(0);
      }


      //categorize leads in statuses
      switch(key.toUpperCase()){
        case 'OPEN':
          this.barChartData[0] = barData;
          break;
        case 'SUCCESS':
          this.barChartData[2] = barData;
          break;
        case 'FAIL':
          this.barChartData[3] = barData;
          break;
        default:
          for(var k = 0; k < barData.data.length; k++){
            this.barChartData[1].data[k] += barData.data[k];
          }
          break;
      }
    }

    //add remaining statuses to the statusWise object
    this.statusService.loadStatuses().subscribe(data => {

      if(data.success){

        var statuses = data.status;

        for(var i = 0; i < statuses.length; i++){

          if(!(statuses[i].name.toUpperCase() in this.statusWise)){
            this.statusWise[statuses[i].name.toUpperCase()] = 0;
            /*
            var barData = {
              data: [],
              label: key
            };
            for(var j = 0; j < this.monthWise.length; j++){
              if(statuses[i].name.toUpperCase() in this.monthWise[j])
                barData.data.push((this.monthWise[j])[key]);
              else
                barData.data.push(0);
            }
            switch(statuses[i].name.toUpperCase()){
              case 'OPEN':
                this.barChartData[0] = barData;
                break;
              case 'SUCCESS':
                this.barChartData[2] = barData;
                break;
              case 'FAIL':
                this.barChartData[3] = barData;
                break;
              default:
                for(var k = 0; k < barData.data.length; k++){
                  this.barChartData[1].data[k] += barData.data[k];
                }
                break;
            }
            */
          }
        }

        //force redraw bar chart
        this.barChartData = JSON.parse(JSON.stringify(this.barChartData));
      }
    });
  }

  //when user clicks on weekly user data analysis use userAnalysUtil utility function to get user analysis data
  onWeeklyUserClick(): void {
      //set display for week wise analysis
      this.userDisplay = "week";

      //set range of date for previous week
      var today = new Date();
      today.setDate(today.getDate()-7);
      today.setHours(0);
      today.setMinutes(0);

      //use user analysis utility function with created range
      this.userAnalysUtil(today, new Date());
  }

  //when user clicks on monthly user data analysis use userAnalysUtil utility function to get user analysis data
  onMonthlyUserClick(): void {
      //set display for monthly analysis
      this.userDisplay = "month";

      //create range of dates for previus month
      var today = new Date();
      today.setDate(today.getDate()-30);
      today.setHours(0);
      today.setMinutes(0);

      //use userAnalysUtil utility function
      this.userAnalysUtil(today, new Date());
  }

  //when user clicks on quaterly user data analysis use userAnalysUtil utility function to get user analysis data
  onQuaterlyUserClick(): void {
    //set display for quaterly analysis
    this.userDisplay = "quarter";

    //create range of dates for current quarter
    var today = new Date();
    var quarter = Math.floor(today.getMonth()/3);
    var year = today.getFullYear();

    //use user analysis utility function to get analysis data
    this.userAnalysUtil(new Date(year, quarter*3, 1, 0, 0, 0, 0), new Date(year, quarter*3+2, 30, 0, 0, 0, 0));
  }

  //when user wants over all userwise analysis
  onOverallClick(): void {

    //set display for overall analysis
    this.userDisplay = "all";
    this.userWise = {};

    //iterate through all leads
    for(var i = 0; i < this.leads.length; i++){
      var lead = this.leads[i];

      //create userWise object for generating bar chart data
      //if user exist in userWise object then add or increment status wise lead details in that user object
      if(lead.userid in this.userWise){
        var user = this.userWise[lead.userid];
        if(lead.status.toUpperCase() in user){
          user[lead.status.toUpperCase()]++;
          user['assigned']++;
        }else{
          user[lead.status.toUpperCase()] = 1;
          user['assigned']++;
        }
      }else{
        //add new user to userwise object and initialize it with current lead status to 1;
        this.userWise[lead.userid] = {};
        var user = this.userWise[lead.userid];
        user[lead.status.toUpperCase()] = 1;
        user['name'] = lead.assignment;
        user['assigned'] = 1;
      }
    }

    //add details of remaining user
    if(this.userService.loadUserData().type === 'admin') {

      //if logged in user is admin get details of all users
      this.userService.getAllUsers().subscribe(data => {

        if(data.success){

          var users = data.users;

          for(var i = 0; i < users.length; i++){

            if(!(users[i]._id in this.userWise)){
              this.userWise[users[i]._id] = {};
              var user = this.userWise[users[i]._id];
              user.OPEN = 0;
              user.WORKING = 0;
              user.SUCCESS = 0;
              user.FAIL = 0;
              user.name = users[i].firstName+" "+users[i].lastName;
              user.assigned = 0;
            }

          }

        }

      });

    }else{
      //if user is back office user then get only its details
      var user = this.userService.loadUserData();
      if(!(user._id in this.userWise)){
        this.userWise[user._id] = {};
        this.userWise[user._id].OPEN = 0;
        this.userWise[user._id].SUCCESS = 0;
        this.userWise[user._id].WORKING = 0;
        this.userWise[user._id].FAIL = 0;
        this.userWise[user._id].name = user.name;
        this.userWise[user._id].assigned = 0;
      }
    }
  }

  //utility function get userwise details of leads
  userAnalysUtil(startDate: Date, endDate: Date): void {
    //reset userWise details
    this.userWise = {};

    //iterate through all leads
    for(var i = 0; i < this.leads.length; i++){
      var lead = this.leads[i];
      var leadDate = new Date(Date.parse(lead.nextDate));

      //compare lead date with given range of date
      if(this.compareDates(leadDate, startDate) >= 0 && this.compareDates(leadDate, endDate) < 0){

        //if user exists in userWise object then add or increment lead details by status wise in user object
        if(lead.userid in this.userWise){
          var user = this.userWise[lead.userid];
          if(lead.status.toUpperCase() in user){
            user[lead.status.toUpperCase()]++;
            user['assigned']++;
          }else{
            user[lead.status.toUpperCase()] = 1;
            user['assigned']++;
          }
        }else{

          //add new user to userWise object
          this.userWise[lead.userid] = {};
          var user = this.userWise[lead.userid];
          user[lead.status.toUpperCase()] = 1;
          user['name'] = lead.assignment;
          user['assigned'] = 1;
        }
      }
    }

    //get details of remaining users if you are admin
    if(this.userService.loadUserData().type === 'admin') {

      this.userService.getAllUsers().subscribe(data => {

        if(data.success){
          var users = data.users;

          for(var i = 0; i < users.length; i++){
            if(!(users[i]._id in this.userWise)){
              this.userWise[users[i]._id] = {};
              var user = this.userWise[users[i]._id];
              user.OPEN = 0;
              user.WORKING = 0;
              user.SUCCESS = 0;
              user.FAIL = 0;
              user.name = users[i].firstName+" "+users[i].lastName;
              user.assigned = 0;
            }
          }
        }
      });
    }else{
      //if logged in user is back office user then only get his/her detail
      var user = this.userService.loadUserData();
      if(!(user._id in this.userWise)){
        this.userWise[user._id] = {};
        this.userWise[user._id].OPEN = 0;
        this.userWise[user._id].SUCCESS = 0;
        this.userWise[user._id].WORKING = 0;
        this.userWise[user._id].FAIL = 0;
        this.userWise[user._id].name = user.name;
        this.userWise[user._id].assigned = 0;
      }
    }
  }

  //utility function to compare two dates
  compareDates(date1: Date, date2: Date) {
    if(date1.getFullYear() > date2.getFullYear())
      return 1;
    else if(date1.getFullYear() < date2.getFullYear())
      return -1;
    else if(date1.getMonth() > date2.getMonth())
      return 1;
    else if(date1.getMonth() < date2.getMonth())
      return -1;
    else if(date1.getDate() > date2.getDate())
      return 1;
    else if(date1.getDate() < date2.getDate())
      return -1;
    else
      return 0;
  }

  public chartClicked(evt) {}

  public chartHovered(evt) {}
}
