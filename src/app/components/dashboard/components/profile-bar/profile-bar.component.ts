//the top information bar which contains name, logout button and nav toggle button
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { OnlineService } from '../../services/online.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'profile-bar',
  templateUrl: './profile-bar.component.html',
  styleUrls: ['./profile-bar.component.css']
})

export class ProfileBarComponent {

  constructor(
	  private userService: UserService,
	  private flashMessage: FlashMessagesService,
	  private router: Router,
    private onlineService: OnlineService
  ) { }

	@Input()
	name: string = '';

	@Output()
	myevent = new EventEmitter();


  //Logout the user
	onLogoutClick(): void {
    this.onlineService.getSocket().disconnect();
    this.userService.logout().subscribe(response => {
      if(response.success){
				this.userService.clearStorage();
				this.flashMessage.show(response.msg, {cssClass: 'alert-success', timeout: 1500});
				this.router.navigate(['/login']);
			}
		});
    this.router.navigate(['/login']);
	}

  //handle nav collapse and expand
	fireEvent(): void {
		this.myevent.next(['a']);
	}

  onAddLeadClick(): void {
    this.router.navigate(['/dashboard/lead/add']);
  }
}
