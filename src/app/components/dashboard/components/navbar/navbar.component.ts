//handle navigation bar
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { Config } from '../../../../app.config';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
	toggleLeft = true;
  userType = '';
  shortName: string = Config.COMPANY_NAME_SHORT;

	@Input()
	collapseNav: boolean = false;

	@Output()
	expandevent = new EventEmitter();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit() {
    var user = this.userService.loadUserData();
    this.userType = user.type;
  }

  //handle arrow icon of master management menu
	toggleArrow() {
		this.toggleLeft = !this.toggleLeft;
		if(this.collapseNav)
			this.expandevent.next(['']);
	}

  //navigate to specific route based on clicks
  onPasswordClick(): void {
    this.router.navigate(['/dashboard/changepassword']);
  }

  onStatusClick(): void {
    this.router.navigate(['/dashboard/master/status']);
  }

  onSourceClick(): void {
    this.router.navigate(['/dashboard/master/source']);
  }

  onReqClick(): void {
    this.router.navigate(['/dashboard/master/requirement']);
  }

  onRoleClick(): void {
    this.router.navigate(['/dashboard/master/role']);
  }

  onLeadMngmntClick(): void {
    this.router.navigate(['/dashboard/lead']);
  }

  onUserMngmntClick(): void {
    this.router.navigate(['/dashboard/user']);
  }

  onReportClick(): void {
    this.router.navigate(['/dashboard/report']);
  }

  onDashboardClick(): void {
    this.router.navigate(['/dashboard']);
  }
}
