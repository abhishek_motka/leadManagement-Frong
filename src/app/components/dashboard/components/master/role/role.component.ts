import { Component, OnInit } from '@angular/core';
import { RoleService } from '../../../services/master/role.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';
import { FocusAnimation, MoveAnimation } from '../../../../../animation/my-animations';


@Component({
  templateUrl: './role.component.html',
  styleUrls: [
    '../../../dashboard-master-common.css',
    './role.component.css'
 ],
 animations:[
		  FocusAnimation,
		  MoveAnimation
		]
})
export class RoleComponent implements OnInit {
  state: string = 'active';
  roles = [];
  newRole = {
    name: ""
  };

  //filter for status filter
  filter = {
    name: '',
    status: ''
  }

  //filter for order by pipe
  sortFilter = {
      type: '',
      order: undefined,
      fields: []
  };

  //data for pagination
  limits = [5, 10, 15];
  listLimit: number = +this.limits[0];
  page = 1;

  constructor(
    private roleService: RoleService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {

    //load all roles from the databse
    this.roleService.loadRoles().subscribe(data => {
      if(data.success){
        this.roles = data.roles;
        this.roles.sort();
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in retrieving roles.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //when reset button is clicked
  onResetAllClick(form: NgForm): void {

    //delete all roles from the server
    this.roleService.resetRoles().subscribe(data => {

      if(data.success){

        //update the local array
        this.roles = [];

        //reset form
        this.newRole.name = "";
        document.getElementById('roleTextBox').blur();
        form.reset();

        this.flashMessage.show("All roles data is reset.", {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in reseting roles data.", {cssClass: 'alert-danger', timeout: 1500});
      }
    })
  }

  //when add role form is submitted
  onFormSubmit(form: NgForm): void {

    var role = {
      name: this.newRole.name.toUpperCase(),
      status: true
    }

    //add new role to database using service
    this.roleService.addRole(role).subscribe(data => {

      if(data.success){
        //on success update local array
        this.roles.push(role);
        this.roles = this.roles.sort().map(function(value){
          value.name = value.name.toUpperCase();
          return value;
        });

        //reset form
        this.newRole.name = "";
        document.getElementById('roleTextBox').blur();
        form.reset();

      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Unable to add new Role.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //when user clicks trash icon execute this function
  onDeleteClick(role): void{
    var delRole = {
      name: role.name.toUpperCase()
    };

    //delete role from the server using role service
    this.roleService.deleteRole(delRole).subscribe(data => {

      if(data.success){

        //on success update local array by creating new array
        var newRoles = [];
        for(var nrole of this.roles)
          if(nrole.name.toUpperCase() !== delRole.name.toUpperCase())
            newRoles.push(nrole);
        this.roles = newRoles;

      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Unable to delete role.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //function to change the status of the role
  onStatusChange(role) {
    var index = this.roles.indexOf(role);
    role = this.roles[index];

    var updateRole = {
      updateName: role.name.toUpperCase(),
      status: !role.status
    }

    //change status of the role by updating the role
    this.roleService.updateRole(updateRole).subscribe(data => {
      if(data.success){
        this.roles[index].status = !this.roles[index].status;
        this.flashMessage.show("Status of the role updated.", {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in updating the role status.", {cssClass: 'alert-danger', timeout: 1500});
        this.roles[index].status = this.roles[index].status;
      }
    });
  }

  //
  //functions to set filter value for order by pipe
  //

  onNameSortClick(): void {
    this.sortUtil(['name'], 'string');
  }

  onStatusSortClick(): void {
    this.sortUtil(['status'], 'boolean');
  }

  sortUtil(fields: any[], type): void {
      var applied = false;
      for(var i = 0; i < fields.length; i++){
        if(this.sortFilter.fields.indexOf(fields[i]) < 0){
          this.sortFilter.type = type;
          this.sortFilter.order = true;
          this.sortFilter.fields = fields;
          applied = true;
        }
      }
      if(!applied)
        this.sortFilter.order = !this.sortFilter.order;
  }
}
