//component for status master management
import { Component, OnInit } from '@angular/core';
import { StatusService } from '../../../services/master/status.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';
import { FocusAnimation, MoveAnimation } from '../../../../../animation/my-animations';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: [
    '../../../dashboard-master-common.css',
    './status.component.css'
  ],
 animations:[
		  FocusAnimation,
		  MoveAnimation
		]
})
export class StatusComponent implements OnInit {
  state: string = 'active';
  statuses = [];

  //to display update form
  updateStatusObj = null;

  newStatus = {
    status: "",
    order: ""
  };

  //filter for status filter
  filter = {
    name: '',
    status: ''
  }

  //filter for order by pipe
  sortFilter = {
    type: '',
    order: undefined,
    fields: []
  }

  //data for pagination
  limits = [5, 10, 15];
  listLimit: number = +this.limits[0];
  page = 1;

  constructor(private statusService: StatusService,  private flashMessage: FlashMessagesService) { }

  ngOnInit() {
    //on creation load all status details
    this.statusService.loadStatuses().subscribe(data => {
      if(data.success){
        this.statuses = data.status;

        //sort based on order, capitalize names
        this.statuses.sort(function(prev, next){
          prev.name = prev.name.toUpperCase();
          return prev.order - next.order;
        });
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in retrieving status names.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //on edit icon click
  //change update object to the status clicked
  //update form fields relatively
  onEditClick(status): void {
    this.updateStatusObj = status;
    this.newStatus.status = this.updateStatusObj.name;
    this.newStatus.order = this.updateStatusObj.order;
  }

  //on reset button click
  onResetAllClick(form: NgForm): void {

    //delete all statuses from the server
    this.statusService.resetStatus().subscribe(data => {

      if(data.success){
        //set local array to empty array
        this.statuses = [];
        this.updateStatusObj = null;
        this.newStatus.status = "";
        this.newStatus.order = "";

        //remove focus from the input fields of form
        var elements = document.getElementsByTagName('input');
        for(var i = 0; i < elements.length; i++)
          elements.item(i).blur();
        form.reset();
        this.flashMessage.show("All status data is reset.", {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in reseting status data.", {cssClass: 'alert-danger', timeout: 1500});
      }
    })
  }

  //when update form is submitted
  onUpdateClick(form: NgForm): void {

    //create new status object for update
    var updateStatus = {
      updateName: this.updateStatusObj.name,
      name: this.newStatus.status,
      order: this.newStatus.order
    };

    //update status on server using service
    this.statusService.updateStatus(updateStatus).subscribe(data => {
      if(data.success){

        //update local array for display changes
        var index = this.statuses.indexOf(this.updateStatusObj);
        this.updateStatusObj.name = updateStatus.name;
        this.updateStatusObj.order = updateStatus.order;
        this.statuses[index] = this.updateStatusObj;

        this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 1500});

        //reset form fields
        this.updateStatusObj = null;
        this.newStatus.status = "";
        this.newStatus.order = "";

        //remove focus from form
        var elements = document.getElementsByTagName('input');
        for(var i = 0; i < elements.length; i++)
          elements.item(i).blur();
        form.reset();
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in updating status.", {cssClass: 'alert-danger', timeout: 1500});
        this.updateStatusObj = null;
      }
    })
  }

  //when reset form click, remove focus from form and reset it to prestine state
  onResetClick(form: NgForm): void {
    this.newStatus.status = "";
    this.newStatus.order = "";
    var elements = document.getElementsByTagName('input');
    for(var i = 0; i < elements.length; i++)
      elements.item(i).blur();
    form.reset();
  }

  //add status form submitted
  onFormSubmit(form: NgForm): void {
    var status = {
      name: this.newStatus.status.toUpperCase(),
      order: this.newStatus.order,
      status: true
    };

    //add new status using service
    this.statusService.addStatus(status).subscribe(data => {

      if(data.success){
        //add status to local array and sort
        this.statuses.push(status);
        this.statuses.sort(function(prev, next) {
          prev.name = prev.name.toUpperCase();
          return prev.order-next.order;
        });

        this.statuses = this.statuses.filter(function(value) {
          return true;
        });

        //reset form, remove focus
        this.newStatus.status = "";
        this.newStatus.order = "";

        var elements = document.getElementsByTagName('input');
        for(var i = 0; i < elements.length; i++)
          elements.item(i).blur();

        form.reset();

      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Unable to add new Status.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //on trash icon click
  onDeleteClick(status): void{
    var delStatus = {
      name: status.name.toUpperCase()
    };

    //reset form
    this.updateStatusObj = null;
    this.newStatus.status = '';
    this.newStatus.order = '';

    //delete using service
    this.statusService.deleteStatus(delStatus).subscribe(data => {
      if(data.success){

        //remove status from local array  by creating new array
        var newStatuses = [];
        for(var nstatus of this.statuses)
          if(nstatus.name.toUpperCase() !== delStatus.name.toUpperCase())
            newStatuses.push(nstatus);

        this.statuses = newStatuses;
        this.statuses.sort(function(prev, next){
          return prev.order-next.order;
        });

      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Unable to delete status.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //when status of status is changed
  onStatusChange(status) {
    var index = this.statuses.indexOf(status);
    status = this.statuses[index];

    var updateStatus = {
      updateName: status.name.toUpperCase(),
      status: !status.status
    }

    //update status using service
    this.statusService.updateStatus(updateStatus).subscribe(data => {
      if(data.success){
        this.statuses[index].status = !this.statuses[index].status;
        this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in updating the status.", {cssClass: 'alert-danger', timeout: 1500});
        this.statuses[index].status = this.statuses[index].status;
      }
    });
  }


  //functions for sorting of data based on click in table
  onNameSortClick(): void {
    this.sortUtil(['name'], 'string');
  }

  onStatusSortClick(): void {
    this.sortUtil(['status'], 'boolean');
  }

  onOrderSortClick(): void {
    this.sortUtil(['order'], 'number');
  }

  sortUtil(fields: any[], type): void {
      var applied = false;
      for(var i = 0; i < fields.length; i++){
        if(this.sortFilter.fields.indexOf(fields[i]) < 0){
          this.sortFilter.type = type;
          this.sortFilter.order = true;
          this.sortFilter.fields = fields;
          applied = true;
        }
      }
      if(!applied)
        this.sortFilter.order = !this.sortFilter.order;
  }
}
