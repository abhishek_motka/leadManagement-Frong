import { Component, OnInit } from '@angular/core';
import { SourceService } from '../../../services/master/source.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';
import { FocusAnimation, MoveAnimation } from '../../../../../animation/my-animations';


@Component({
  selector: 'app-source',
  templateUrl: './source.component.html',
  styleUrls: [
    '../../../dashboard-master-common.css',
    './source.component.css'
  ],
 animations:[
		  FocusAnimation,
		  MoveAnimation
		]
})
export class SourceComponent implements OnInit {
  state: string = 'active';
  sources = [];
  newSource = {
    name: ""
  };

  //filter for status filter
  filter = {
    name: '',
    status: ''
  }

  //filter for order by pipe
  sortFilter = {
    type: '',
    order: undefined,
    fields: []
  }

  //data for pagination
  limits = [5, 10, 15];
  listLimit: number = +this.limits[0];
  page = 1;

  constructor(
    private sourceService: SourceService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
    //load all sources from the server
    this.sourceService.loadSources().subscribe(data => {
      if(data.success){
        this.sources = data.sources;
        this.sources.sort();
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in retrieving sources.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //on reset button clicked
  onResetAllClick(form: NgForm): void {

    //delete all sources from database using service
    this.sourceService.resetSources().subscribe(data => {
      if(data.success){

        //reset form
        this.sources = [];
        this.newSource.name = "";
        document.getElementById('sourceTextBox').blur();
        form.reset();
        this.flashMessage.show("All sources data is reset.", {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in reseting source data.", {cssClass: 'alert-danger', timeout: 1500});
      }
    })
  }

  //when add source form is submitted
  onFormSubmit(form: NgForm): void {

    var source = {
      name: this.newSource.name.toUpperCase(),
      status: true
    }

    //add new source using service
    this.sourceService.addSource(source).subscribe(data => {
      if(data.success){
        //update local array
        this.sources.push(source);
        this.sources = this.sources.sort().map(function(value){
          value.name = value.name.toUpperCase();
          return value;
        });

        //reset form
        this.newSource.name = "";
        document.getElementById('sourceTextBox').blur();
        form.reset();
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Unable to add new Source.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //when trash icon is clicked
  onDeleteClick(source): void{
    var delSource = {
      name: source.name.toUpperCase()
    };

    //delete particular source using source service
    this.sourceService.deleteSource(delSource).subscribe(data => {
      if(data.success){

        //update local array by creating new array
        var newSources = [];
        for(var nsource of this.sources)
          if(nsource.name.toUpperCase() !== delSource.name.toUpperCase())
            newSources.push(nsource);
        this.sources = newSources;

      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Unable to delete source.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //when status of source is changed
  onStatusChange(source) {
    var index = this.sources.indexOf(source);
    source = this.sources[index];

    var updateSource = {
      updateName: source.name.toUpperCase(),
      status: !source.status
    }

    //update status on server using source service
    this.sourceService.updateSource(updateSource).subscribe(data => {
      if(data.success){
        this.sources[index].status = !this.sources[index].status;
        this.flashMessage.show("Status of source updated.", {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in updating the source status.", {cssClass: 'alert-danger', timeout: 1500});
        this.sources[index].status = this.sources[index].status;
      }
    });
  }

  //
  //functions to set filter for order by pipe
  //

  onNameSortClick(): void {
    this.sortUtil(['name'], 'string');
  }

  onStatusSortClick(): void {
    this.sortUtil(['status'], 'boolean');
  }

  sortUtil(fields: any[], type): void {
      var applied = false;
      for(var i = 0; i < fields.length; i++){
        if(this.sortFilter.fields.indexOf(fields[i]) < 0){
          this.sortFilter.type = type;
          this.sortFilter.order = true;
          this.sortFilter.fields = fields;
          applied = true;
        }
      }
      if(!applied)
        this.sortFilter.order = !this.sortFilter.order;
  }
}
