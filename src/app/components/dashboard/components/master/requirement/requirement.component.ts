import { Component, OnInit } from '@angular/core';
import { RequirementService } from '../../../services/master/requirement.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';
import { FocusAnimation, MoveAnimation } from '../../../../../animation/my-animations';


@Component({
  templateUrl: './requirement.component.html',
  styleUrls: [
    '../../../dashboard-master-common.css',
    './requirement.component.css'
  ],
 animations:[
		  FocusAnimation,
		  MoveAnimation
		]
})

export class RequirementComponent implements OnInit {
  state: string = 'active';
  requirements = [];
  newRequirement = {
    name: ""
  };

  //filter for status filter
  filter = {
    name: '',
    status: ''
  }

  //filter for order by pipe
  sortFilter = {
      type: '',
      order: undefined,
      fields: []
  };

  //data for pagination
  limits = [5, 10, 15];
  listLimit: number = +this.limits[0];
  page = 1;

  constructor(
    private requirementService: RequirementService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
    //load all requirements from the server
    this.requirementService.loadRequirements().subscribe(data => {
      if(data.success){
        this.requirements = data.requirements;
        this.requirements.sort();
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in retrieving requirements.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //when user clicks reset button
  onResetAllClick(form: NgForm): void {

    //delete all requirements from the server
    this.requirementService.resetRequirements().subscribe(data => {

      if(data.success){

        //on success update the local array
        this.requirements = [];

        //reset form
        this.newRequirement.name = "";
        document.getElementById('requirementTextBox').blur();
        form.reset();

        this.flashMessage.show("All requirements data is reset.", {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in reseting requirements data.", {cssClass: 'alert-danger', timeout: 1500});
      }
    })
  }

  //when add rquirement form is submitted
  onFormSubmit(form: NgForm): void {

    var requirement = {
      name: this.newRequirement.name.toUpperCase(),
      status: true
    }

    //add new requirement using service
    this.requirementService.addRequirement(requirement).subscribe(data => {

      if(data.success){

        //on success update local array and sort it
        this.requirements.push(requirement);
        this.requirements = this.requirements.sort().map(function(value){
          value.name = value.name.toUpperCase();
          return value;
        });

        //reset form
        this.newRequirement.name = "";
        document.getElementById('requirementTextBox').blur();
        form.reset();

      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Unable to add new Requirement.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //when user clicks on trash icon
  onDeleteClick(requirement): void{
    var delRequirement = {
      name: requirement.name.toUpperCase()
    };

    //delete the specific requirement using service
    this.requirementService.deleteRequirement(delRequirement).subscribe(data => {
      if(data.success){

        //on success update local array by creating new array
        var newRequirements = [];
        for(var nrequirement of this.requirements)
          if(nrequirement.name.toUpperCase() !== delRequirement.name.toUpperCase())
            newRequirements.push(nrequirement);
        this.requirements = newRequirements;

      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Unable to delete requirement.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //when user changes the status of the requirement
  onStatusChange(requirement) {
    var index = this.requirements.indexOf(requirement);
    requirement = this.requirements[index];

    var updateRequirement = {
      updateName: requirement.name.toUpperCase(),
      status: !requirement.status
    }

    //change status of requirement by updating the requirement details
    this.requirementService.updateRequirement(updateRequirement).subscribe(data => {
      if(data.success){
        this.requirements[index].status = !this.requirements[index].status;
        this.flashMessage.show("Status of requirement updated.", {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in updating the requirement status.", {cssClass: 'alert-danger', timeout: 1500});
        this.requirements[index].status = this.requirements[index].status;
      }
    });
  }

  //functions to set filter for orderby pipe
  onNameSortClick(): void {
    this.sortUtil(['name'], 'string');
  }

  onStatusSortClick(): void {
    this.sortUtil(['status'], 'boolean');
  }

  sortUtil(fields: any[], type): void {
      var applied = false;
      for(var i = 0; i < fields.length; i++){
        if(this.sortFilter.fields.indexOf(fields[i]) < 0){
          this.sortFilter.type = type;
          this.sortFilter.order = true;
          this.sortFilter.fields = fields;
          applied = true;
        }
      }
      if(!applied)
        this.sortFilter.order = !this.sortFilter.order;
  }
}
