import { Component, OnInit } from '@angular/core';
import { SourceService } from '../../services/master/source.service';
import { RequirementService } from '../../services/master/requirement.service';
import { StatusService } from '../../services/master/status.service';
import { FollowupsService } from '../../services/followups.service';
import { UserService } from '../../../../services/user.service';
import { LeadService } from '../../services/lead.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';
import { FocusAnimation, MoveAnimation, MoveUpAnimation } from '../../../../animation/my-animations';


@Component({
  selector: 'app-lead',
  templateUrl: './lead.component.html',
  styleUrls: [
    '../../dashboard-master-common.css',
    './lead.component.css'
  ],
  animations:[
      FocusAnimation,
      MoveAnimation,
      MoveUpAnimation
    ]
})
export class LeadComponent implements OnInit {
  state: string = 'active';
  users = [];
  leads = [];
  sources = [];
  requirements = [];
  roles = [];
  statuses = [];
  followUps = [];

  //when true show form
  showAddForm = false;

  //when true show update form
  updateLead = false;

  updateUsername = '';

  filter = {status: true};

  //to store leads and follow up for add and view log functionalities
  modalFollowUp = null;
  modalLead = null;

  userTypes = [
    {name: 'admin', value: 'Admin'},
    {name: 'user', value: 'Back Office'}
  ];

  newLead = {
    fName: '',
    lName: '',
    mobile: '',
    address: '',
    source: '',
    requirement: '',
    status: '',
    assignment: '',
    ndate: new Date(),
    followups: [],
    notes: ''
  };

  //when true show follow up form in modal
  showFollowupForm = false;

  newFollowUp = {
    details: '',
    ndate: new Date(),
    status: '',
    userid: ''
  };

  //filter for lead filter pipe
  leadFilter = {
    created: undefined,
    name: '',
    mobile: '',
    source: '',
    requirement: '',
    status: '',
    assignment: '',
    ndate: ''
  };

  //filter for order by pipe
  sortFilter = {
      type: '',
      order: undefined,
      fields: []
  };

  //options for datepicker used
  datePickerOptions = {
    clearBtn: true,
    autoclose: true,
    assumeNearbyYear: true,
    format:'dd-mm-yyyy',
    todayBtn: 'linked',
    todayHighlight: true
  };

  //data for pagination
  limits = [10, 20, 50];
  listLimit: number = +this.limits[0];
  page = 1;

  constructor(
    private sourceService: SourceService,
    private requirementService: RequirementService,
    private statusService: StatusService,
    private userService: UserService,
    private leadService: LeadService,
    private router: Router,
    private route: ActivatedRoute,
    private followUpService: FollowupsService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
    //load all sources details
    this.sourceService.loadSources().subscribe(data => {
      if(data.success)
        this.sources = data.sources;
      else
        this.sources = [];
    });

    //load all requirements from the server
    this.requirementService.loadRequirements().subscribe(data => {
      if(data.success)
        this.requirements = data.requirements;
      else
        this.requirements = [];
    });

    //load all statuses from the database
    this.statusService.loadStatuses().subscribe(data => {
      if(data.success)
        this.statuses = data.status;
      else
        this.statuses = [];
    });

    //load all user data from the server
    this.userService.getAllUsers().subscribe(data => {
      if(data.success)
        this.users = data.users;
      else
        this.users = [];
    });

    //load all leads from the database
    this.leadService.loadLeads().subscribe(data => {
      if(data.success)
        this.leads = data.leads;
      else
        this.leads = [];

      //add overdueStatus fields based on next followUps date
      for(var i = 0; i < this.leads.length; i++){

        var date = new Date(Date.parse(this.leads[i].nextDate));
        var today = new Date();

        if(date < today)
          this.leads[i].overdueStatus = "overdue";
        else {

          today.setDate(today.getDate()+90);

          if(date < today)
            this.leads[i].overdueStatus = "near_future";
          else
            this.leads[i].overdueStatus = "safe";
        }
      }
    });

    //extraxt data from the route path
    this.route.data.subscribe(data => {

      //if show add form true then show form
      if(data.showAddForm)
        this.showAddForm = true;
      else
        this.showAddForm = false;

      //if updateLead is true then show update form instead of add lead form
      if(this.showAddForm && data.updateLead) {

          this.updateLead = data.updateLead;

          //get details of specified lead and set the fields of the form
          this.route.params.subscribe(params => {
            if(params['id']){

              //get lead details using lead service
              this.leadService.getLead(params['id']).subscribe(data => {
                if(data.success){
                  this.newLead.fName = data.lead.fName;
                  this.newLead.lName = data.lead.lName;
                  this.newLead.mobile = data.lead.mobile;
                  this.newLead.address = data.lead.address;
                  this.newLead.source = data.lead.source;
                  this.newLead.assignment = data.lead.userid;
                  this.newLead.requirement = data.lead.requirement;
                  this.newLead.notes = data.lead.notes;
                  this.newLead.status = data.lead.status;
                  var date = new Date(Date.parse(data.lead.nextDate));
                  this.newLead.ndate = date;
                }else{
                  this.flashMessage.show("Error in loading lead data", {cssClass: 'alert alert-danger', timeout: 1500});
                }
              });
            }else{
              this.flashMessage.show("Invalid path to edit the lead.", {cssClass: 'alert alert-danger', timeout: 1500});
            }
          });
      }
    });
  }

  //when add button is clicked, navvigate to add lead path
  onAddLeadClick() {
    this.router.navigate(['/dashboard/lead/add']);
  }

  //utility function to restore form
  resetForm(form: NgForm): void {
    var elements = document.getElementsByTagName('input');
    for(var i = 0; i < elements.length; i++)
      elements.item(i).blur();
    form.reset();

    this.newLead.fName = '';
    this.newLead.lName = '';
    this.newLead.mobile = '';
    this.newLead.address = '';
    this.newLead.source = '';
    this.newLead.assignment = this.users[0]._id;
    this.newLead.requirement = '';
    this.newLead.notes = '';
    this.newLead.status = '';
    this.newLead.ndate = new Date();
  }

  //when cancel button is clicked in form, navigate to leads list
  onCancelClick(form: NgForm): void {
    this.resetForm(form);
    this.router.navigate(['/dashboard/lead']);
  }

  //when reset button is clicked
  onResetAllClick(): void {

    //delete all leads from the server using lead service
    this.leadService.resetLeads().subscribe(data => {
      if(data.success){
          this.flashMessage.show("All leads data is reset", {cssClass: 'alert-success', timeout: 1500});
          this.leads = [];
          this.followUpService.removeFollowUps().subscribe(() => {});
      }else {
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in resetting the leads data", {cssClass: 'alert-danger', timeout: 1500});
      }
    });
  }

  //function executes when user submits add lead form
  onSubmit(form: NgForm): void {
    var addLead = {
      fName: this.newLead.fName,
      lName: this.newLead.lName,
      mobile: this.newLead.mobile,
      address: this.newLead.address,
      src: this.newLead.source,
      req: this.newLead.requirement,
      userid: this.newLead.assignment,
      status: this.newLead.status,
      note: this.newLead.notes,
      ndate: this.newLead.ndate,
      assignment: '',
      overdueStatus: '',
      followUps: []
    };

    this.userService.getUser(this.newLead.assignment).subscribe(data => {
      if(data.success)
        addLead.assignment = data.user.firstName + " " + data.user.lastName;

      //add new lead using service
      this.leadService.addLead(addLead).subscribe(data => {
        if(data.success){
          this.flashMessage.show("New lead added successfully", {cssClass: 'alert-success', timeout: 1500});

          //add overdueStatus field to it
          var date = addLead.ndate;
          var today = new Date();

          if(date < today)
            addLead.overdueStatus = "overdue";
          else {
            today.setDate(today.getDate()+90);
            if(date < today)
              addLead.overdueStatus = "near_future";
            else
              addLead.overdueStatus = "safe";
          }
          this.leads.push(addLead);

          //reset form
          this.resetForm(form);
        }else{
          if(data.msg)
            this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
          else
            this.flashMessage.show("Error in adding new lead", {cssClass: 'alert-danger', timeout: 1500});
        }
      });
    });
  }

  //when user clicks trash icon
  onDeleteClick(lead): void{

    //lead specific lead from the server
    this.leadService.deleteLead(lead._id).subscribe(data => {
      if(data.success){

        //update local array by creating new array
        var newLeads = [];
        var len = this.leads.length;
        for(var i = 0; i < len; i++){
          if(this.leads[i]._id !== lead._id)
            newLeads.push(this.leads[i]);
        }
        this.leads = newLeads;

        //delete followUps for that lead
        this.followUpService.resetFollowUps(lead._id).subscribe(res => {});
        this.flashMessage.show("Lead deleted successfully", {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg){
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        }else{
          this.flashMessage.show("Error in deleting the lead.", {cssClass: 'alert-danger', timeout: 1500});
        }
      }
    });
  }

  //when edit is clicked, navigate to edit path
  onEditClick(lead): void {
    this.router.navigate(['/dashboard/lead/edit',lead._id]);
  }

  //when update lead form is submitted
  onUpdateClick(): void {

    //create update lead object
    var updateLead = {
      id: this.route.snapshot.params['id'],
      fName: this.newLead.fName,
      lName: this.newLead.lName,
      mobile: this.newLead.mobile,
      address: this.newLead.address,
      src: this.newLead.source,
      req: this.newLead.requirement,
      userid: this.newLead.assignment,
      status: this.newLead.status,
      note: this.newLead.notes,
      ndate: this.newLead.ndate,
      assignment: '',
      followUps: []
    };

    this.userService.getUser(this.newLead.assignment).subscribe(data => {
      if(data.success)
        updateLead.assignment = data.user.firstName + " " + data.user.lastName;

      //update lead using lead service
      this.leadService.updateLead(updateLead).subscribe(data => {
        if(data.success){
          this.flashMessage.show("Lead data updated successfully", {cssClass: 'alert-success', timeout: 1500});

          //on success, update local array for changes
          var len = this.leads.length;
          for(var i = 0; i < len; i++){
            if(this.leads[i]._id === this.route.snapshot.params['id']){
              this.leads[i].fName = updateLead.fName;
              this.leads[i].lName = updateLead.lName;
              this.leads[i].mobile = updateLead.mobile;
              this.leads[i].address = updateLead.address;
              this.leads[i].status = updateLead.status;
              this.leads[i].source = updateLead.src;
              this.leads[i].assignment = updateLead.assignment;
              this.leads[i].requirement = updateLead.req;
              this.leads[i].userid = updateLead.userid;
              this.leads[i].notes = updateLead.note;
              this.leads[i].ndate = updateLead.ndate;
            }
          }
        }else{
          if(data.msg)
            this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
          else
            this.flashMessage.show("Error in updating the lead", {cssClass: 'alert-danger', timeout: 1500});
        }
      });
    });
  }

  //when log button is clicked set that lead as modal lead
  onLogClick(lead): void {
    this.modalLead = lead;

    //get details of all followups of the modal lead
    this.followUpService.getFollowUps(lead._id).subscribe(data => {
      if(data.success){
        this.followUps = data.followups;

        //sort followups by created field
        if(this.followUps.length > 0)
          this.followUps.sort(function(prev, next) {
            return prev.created - next.created;
          });
        this.modalFollowUp = this.followUps[this.followUps.length-1];
      }
    });
  }

  //when add log button is clicked show follow up form
  onAddLogClick(): void {
    this.showFollowupForm = true;
  }

  //utility function to reset follow up form
  resetFollowUpForm(form: NgForm): void {
    var elements = document.getElementsByTagName('input');
    for(var i = 0; i < elements.length; i++)
      elements.item(i).blur();
    form.reset();

    this.newFollowUp.status = '';
    this.newFollowUp.userid = '';
    this.newFollowUp.details = '';
    this.newFollowUp.ndate = new Date();
  }

  //reset followup form and go back to followup list
  onFollowUpCancelClick(form: NgForm): void {
    this.resetFollowUpForm(form);
    this.showFollowupForm = false;
  }

  //when follow up form is submitted
  onFollowUpSubmit(form: NgForm): void {
    var addFollowUp = {
      leadid: this.modalLead._id,
      date: this.newFollowUp.ndate,
      userid: this.newFollowUp.userid,
      status: this.newFollowUp.status,
      detail: this.newFollowUp.details,
      assignment: ''
    };

    this.userService.getUser(this.newFollowUp.userid).subscribe(data => {
      if(data.success)
        addFollowUp.assignment = data.user.firstName+" "+data.user.lastName;

      //add new follow up for the lead
      this.followUpService.addFollowUp(addFollowUp).subscribe(data => {
        if(data.success){
          if(this.followUps.length <= 0 || new Date(data.followUp.date).getTime() > new Date(this.followUps[this.followUps.length-1].date).getTime()){
            var newLead = {
              id: this.modalLead._id,
              status: data.followUp.status,
              ndate: data.followUp.date,
              assignment: data.followUp.assignment,
              userid: data.followUp.userid
            };

            var modalNdate = new Date(Date.parse(this.modalLead.nextDate));
            var followUpDate = new Date(Date.parse(data.followUp.date));

            //if new followup date is larger that next follow up date of lead, update the lead
            if(followUpDate >= modalNdate){
              this.leadService.updateLead(newLead).subscribe(data => {
                if(data.success){

                  //on success update local array by creating nw array
                  var len = this.leads.length;
                  for(var i = 0; i < len; i++){
                    if(this.leads[i]._id === this.modalLead._id){
                      this.leads[i].status = newLead.status;
                      this.leads[i].assignment = newLead.assignment;
                      this.leads[i].userid = newLead.userid;
                      this.leads[i].nextDate = newLead.ndate;

                      //add overdueStatus field
                      var date = new Date(Date.parse(this.leads[i].nextDate));
                      var today = new Date();
                      if(date < today){
                        this.leads[i].overdueStatus = "overdue";
                      }else {
                        today.setDate(today.getDate()+90);
                        if(date < today){
                            this.leads[i].overdueStatus = "near_future";
                        }
                        else{
                            this.leads[i].overdueStatus = "safe";
                        }
                      }
                    }
                  }
                }else{
                  this.flashMessage.show("Error in updating lead data", {cssClass: 'alert-danger', timeout: 1500});
                }
              });
            }
          }

          //update local array and sort
          this.followUps.push(data.followUp);
          this.followUps.sort(function(prev, next) {
            return new Date(prev.created).getTime() - new Date(next.created).getTime();
          });

          this.modalFollowUp = data.followUp;

          this.flashMessage.show("Follow up added successfully", {cssClass: 'alert-success', timeout: 1500});

          //close followup form
          this.showFollowupForm = false;
        }else{
          if(data.msg)
            this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
          else
            this.flashMessage.show("Error in adding follow up", {cssClass: 'alert-danger', timeout: 1500});

          this.showFollowupForm = false;
        }
      });
    });
  }

  //to select anothe followup to display
  onChangeFollowUp(followup): void{
    this.modalFollowUp = followup;
  }

  //functions to set filter for order by pipe, when clicked on labels in the table headers
  onCreatedSortClick(): void {
      this.sortUtil(['created'], 'date');
  }

  onNameSortClick(): void {
    this.sortUtil(['fName', 'lName'], 'string');
  }

  onSourceSortClick(): void {
    this.sortUtil(['source'], 'string');
  }

  onReqSortClick(): void {
    this.sortUtil(['requirement'], 'string');
  }

  onStatusSortClick(): void {
    this.sortUtil(['status'], 'string');
  }

  onAssignmentSortClick(): void {
    this.sortUtil(['assignment'], 'string');
  }

  onFollowupSortClick(): void {
    this.sortUtil(['nextDate'], 'date');
  }

  sortUtil(fields: any[], type): void {
      var applied = false;
      for(var i = 0; i < fields.length; i++){
        if(this.sortFilter.fields.indexOf(fields[i]) < 0){
          this.sortFilter.type = type;
          this.sortFilter.order = true;
          this.sortFilter.fields = fields;
          applied = true;
        }
      }
      if(!applied)
        this.sortFilter.order = !this.sortFilter.order;
  }
}
