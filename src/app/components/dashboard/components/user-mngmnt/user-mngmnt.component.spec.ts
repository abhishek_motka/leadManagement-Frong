import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMngmntComponent } from './user-mngmnt.component';

describe('UserMngmntComponent', () => {
  let component: UserMngmntComponent;
  let fixture: ComponentFixture<UserMngmntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMngmntComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMngmntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
