import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { RoleService } from '../../services/master/role.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FocusAnimation, MoveAnimation, MoveUpAnimation } from '../../../../animation/my-animations';


@Component({
  selector: 'app-user-mngmnt',
  templateUrl: './user-mngmnt.component.html',
  styleUrls: [
    '../../dashboard-master-common.css',
    './user-mngmnt.component.css'
  ],
  animations:[
 		  FocusAnimation,
 		  MoveAnimation,
		  MoveUpAnimation
 		]
})

export class UserMngmntComponent implements OnInit {
  state: string = 'active';
  users = [];

  newUser = {
    fName: '',
    lName: '',
    username: '',
    type: '',
    email: '',
    role: '',
    status: false,
    password: '',
    confirmPassword: ''
  };

  roles = [];

  userTypes = [
    {name: 'admin', value: 'Admin'},
    {name: 'user', value: 'Back Office'}
  ];

  //show form when true, user list otherwise
  showAddForm = false;

  //show edit form, add form otherwise
  updateUser = false;
  updateUsername = '';

  //filter for user filter
  filter = {
    name: '',
    type: '',
    email: '',
    status: ''
  }

  //filter for orderBy filter
  sortFilter = {
      type: "",
      order: true,
      fields: []
  }

  //data for pagination
  limits = [10, 20, 50];
  listLimit: number = +this.limits[0];
  page = 1;

  constructor(
    private userService: UserService,
    private flashMessage: FlashMessagesService,
    private roleService: RoleService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  //executes when component is created
  ngOnInit() {

    //retrieve all users informations
    this.userService.getAllUsers().subscribe(response => {
      if(response.success){
        this.users = response.users;
      }else{
        this.flashMessage.show("Error in retrieving users' data.");
      }
    });

    //retrieve all roles information for the form
    this.roleService.loadRoles().subscribe(response => {
      if(response.success){
        this.roles = response.roles;
        this.roles = this.roles.filter(function(value) {
          return value.status;
        });
        this.roles.sort();
        this.newUser.role = this.roles[0].name;
        this.newUser.type = this.userTypes[0].name;
      }else{
        if(response.msd)
          this.flashMessage.show(response.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in retrieving role information.", {cssClass: 'alert-danger', timeout: 1500});
      }
    });

    //if route matches to add or edit and route has showAddForm data then show form instead of users list
    this.route.data.subscribe(data => {
      if(data.showAddForm)
        this.showAddForm = true;
      else
        this.showAddForm = false;

      //if path is to edit user, get username from the path and get details of that user and set fields accordingly
      if(data.updateUser){

        this.route.params.subscribe(params => {

          if(params['username']){

            this.userService.getUserByUsername(params['username']).subscribe(res => {

              if(res.success){
                this.newUser.fName = res.user.firstName;
                this.newUser.lName = res.user.lastName;
                this.newUser.type = res.user.type;
                this.newUser.username = res.user.username;
                this.newUser.status = res.user.status;
                this.newUser.email = res.user.email;

                if(res.user.role)
                  this.newUser.role = res.user.role;

                this.updateUser = true;
                this.updateUsername = res.user.username;

              }else{
                this.flashMessage.show('Unable to update user.', {cssClass: 'alert-danger', timeout: 1500});
                this.router.navigate(['/dashboard/user']);
              }

            });

          }else{

            this.flashMessage.show('Unable to update user.', {cssClass: 'alert-danger', timeout: 1500});
            this.router.navigate(['/dashboard/user']);

          }
        });

      }
      else
        this.updateUser = false;
    });
  }

  //when add user button is clicked
  onAddUserClick(): void{
    this.router.navigate(['/dashboard/user/add']);
  }

  //helper function to reset forms
  resetForm(form: NgForm): void {
    var elements = document.getElementsByTagName('input');
    for(var i = 0; i < elements.length; i++)
      elements.item(i).blur();
    form.reset();

    this.newUser.fName = "";
    this.newUser.lName = "";
    this.newUser.username = "";
    this.newUser.email = "";
    this.newUser.status = false;
    this.newUser.password = '';
    this.newUser.confirmPassword = "";
    this.newUser.type = this.userTypes[0].name;
  }

  //when clicked on cancel navigate to user list path
  onCancelClick(form: NgForm): void {
    this.resetForm(form);

    this.router.navigate(['/dashboard/user']);
  }

  //when add user form is submitted
  onSubmit(form: NgForm) {
    //create new user object from the input fields
    var addUser = {
      fName: this.newUser.fName,
      lName: this.newUser.lName,
      username: this.newUser.username,
      email: this.newUser.email,
      status: this.newUser.status,
      password: this.newUser.password,
      type: this.newUser.type,
      role: this.newUser.role
    }

    //add role field for only user not for admin
    if(this.newUser.type !== 'user')
      addUser.role = '';

    //use user service to add new user
    this.userService.addUser(addUser).subscribe(data => {
      if(data.success){

        //on success add new user to the users array for client side update
        this.users.push(addUser);

        //show success message
        if(data.msg){
          this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 1500});
        }else{
          this.flashMessage.show("New user added to the system", {cssClass: 'alert-success', timeout: 1500});
        }

        //reset form
        this.resetForm(form);
      }else{
        if(data.msg){
          this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 1500});
        }else{
          this.flashMessage.show("New user added to the system", {cssClass: 'alert-success', timeout: 1500});
        }

        //reset form
        this.resetForm(form);
      }
    });
  }

  //when user update form is submitted
  onUpdateClick(): void {

    //create new user object from the input fields
    var addUser = {
      fName: this.newUser.fName,
      lName: this.newUser.lName,
      username: this.newUser.username,
      email: this.newUser.email,
      status: this.newUser.status,
      type: this.newUser.type,
      role: this.newUser.role
    }

    //use userservice to update the details
    this.userService.updateUserDetails(addUser, this.updateUsername).subscribe(res => {

      if(res.success){
        this.flashMessage.show('User detail updated successfully', {cssClass: 'alert-success', timeout: 1500});

        //if success then iterate through all users and update the details of the user at client side
        var len = this.users.length;
        for(var i = 0; i < len; i++){
          if(this.users[i].username === this.updateUsername){
            this.users[i].fName = addUser.fName;
            this.users[i].lName = addUser.lName;
            this.users[i].username = addUser.username;
            this.users[i].email = addUser.email;
            this.users[i].status = addUser.status;
            this.users[i].type = addUser.type;
            this.users[i].role = this.newUser.role;
            if(addUser.type === 'admin')
              this.users[i].role = '';
          }
        }

        //remove focus from all input fields in the form
        var elements = document.getElementsByTagName('input');
        for(var i = 0; i < elements.length; i++)
          elements.item(i).blur();
      }else{
        //if error show error
        if(res.msg){
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 1500});
        }else{
          this.flashMessage.show("Error in updating the details.", {cssClass: 'alert-danger', timeout: 1500});
        }

        //remove focus from all input fields in the form
        var elements = document.getElementsByTagName('input');
        for(var i = 0; i < elements.length; i++)
          elements.item(i).blur();
      }
    });
  }

  //when clicked on edit icon
  //navigate to edit user path
  onEditClick(user): void {
    this.router.navigate(['/dashboard/user/edit', user.username]);
  }

  //when user clicks on trash icon
  onDeleteClick(user): void {

    //use userservice to delete that perticular user
    this.userService.deleteUser(user.username).subscribe(res => {
      if(res.success){

        //if success in deletion the update array for client side updation
        var newUsers = [];
        var len = this.users.length;
        for(var i = 0; i < len; i++){
          if(this.users[i].username.toLowerCase() !== user.username.toLowerCase())
            newUsers.push(this.users[i]);
        }
        this.users = newUsers;
        this.flashMessage.show("User deleted successfully", {cssClass: 'alert-success', timeout: 1500});
      }else{

        //if error show error
        if(res.msg){
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 1500});
        }else{
          this.flashMessage.show("Error in deleting the user.", {cssClass: 'alert-danger', timeout: 1500});
        }
      }
    });
  }

  //when user clicks reset button
  onResetAllClick(): void {

    //delete all users except admins from the server
    this.userService.resetUserData().subscribe(res => {
      if(res.success){
        this.flashMessage.show("All users data is reset.", {cssClass: 'alert-info', timeout: 1500});
        this.userService.getAllUsers().subscribe(data => {
          if(res.success){
            this.users = data.users;
          }else{
            this.flashMessage.show("Error in reloading the users data.", {cssClass: 'alert-danger', timeout: 1500});
          }
        });
      }else{
        if(res.msg){
          this.flashMessage.show(res.msg, {cssClass: 'alert-danger', timeout: 1500});
        }else{
          this.flashMessage.show("Error in reseting the user data.", {cssClass: 'alert-danger', timeout: 1500});
        }
      }
    });
  }

  //when clicked on status slider to change the status of that user
  onStatusChange(user) {
    var index = this.users.indexOf(user);

    user = this.users[index];

    var updateUser = {
      status: !user.status
    };

    //use userservice to update the details on server
    this.userService.updateUserDetails(updateUser, user.username).subscribe(data => {

      if(data.success){
        //change status of user in array for instant change display
        this.users[index].status = !this.users[index].status;
        this.flashMessage.show(data.msg, {cssClass: 'alert-success', timeout: 1500});
      }else{
        if(data.msg)
          this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});
        else
          this.flashMessage.show("Error in updating the status.", {cssClass: 'alert-danger', timeout: 1500});
        //if error in saving updated status, status remains same
        this.users[index].status = this.users[index].status;
      }
    });
  }

  //when clicked on user name field in table
  onUsernameSortClick(): void {
    this.sortUtil(['firstName', 'lastName'], 'string');
  }

  //set filter for type filed when clicked on type label in table
  onTypeSortClick(): void {
    this.sortUtil(['type'], 'string');
  }

  //to sort data based on email, when clicked on email
  onEmailSortClick(): void {
    this.sortUtil(['email'], 'string');
  }

  //to sort data based on status of the user, set filter data for orderBy pipe
  onStatusSortClick(): void {
    this.sortUtil(['status'], 'boolean');
  }

  //utility function to update the filter of orderBy pipe
  sortUtil(fields: any[], type): void {
      var applied = false;
      for(var i = 0; i < fields.length; i++){
        if(this.sortFilter.fields.indexOf(fields[i]) < 0){
          this.sortFilter.type = type;
          this.sortFilter.order = true;
          this.sortFilter.fields = fields;
          applied = true;
        }
      }
      if(!applied)
        this.sortFilter.order = !this.sortFilter.order;
  }
}
