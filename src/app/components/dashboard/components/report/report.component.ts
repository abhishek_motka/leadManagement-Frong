//report component to display and export reports
import { Component, OnInit } from '@angular/core';
import { SourceService } from '../../services/master/source.service';
import { RequirementService } from '../../services/master/requirement.service';
import { StatusService } from '../../services/master/status.service';
import { FollowupsService } from '../../services/followups.service';
import { UserService } from '../../../../services/user.service';
import { LeadService } from '../../services/lead.service';
import { LeadfilterPipe } from '../../../../pipe/leadfilter.pipe';
import { DatePipe } from '@angular/common';
import { Angular2Csv } from 'angular2-csv';
import { FocusAnimation, MoveUpAnimation } from '../../../../animation/my-animations';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css'],
  animations:[
 		  FocusAnimation,
 		  MoveUpAnimation
 		]
})
export class ReportComponent implements OnInit {
  state: string = 'active';
  leads= [];

  //to store lead for follow ups
  modalLead = null;

  //to display followup detail in the modal
  modalFollowUp = null;

  followUps = [];
  sources = [];
  users = [];
  statuses = [];
  requirements = [];

  //filter data for lead filter
  filter = {
    created: '',
    name: '',
    mobile: '',
    source: '',
    requirement: '',
    status: '',
    assignment: '',
    ndate: ''
  }

  //filter for order by pipe
  sortFilter = {
    type: '',
    order: undefined,
    fields: []
  }

  //data for pagination
  limits = [10, 20, 50];
  listLimit: number = +this.limits[0];
  page = 1;

  constructor(
    private sourceService: SourceService,
    private requirementService: RequirementService,
    private statusService: StatusService,
    private userService: UserService,
    private leadService: LeadService,
    private followUpService: FollowupsService
  ) { }

  ngOnInit() {
    //retrieve source data from the server
    this.sourceService.loadSources().subscribe(data => {
      if(data.success)
        this.sources = data.sources;
      else
        this.sources = [];
    });

    //get requirements data
    this.requirementService.loadRequirements().subscribe(data => {
      if(data.success)
        this.requirements = data.requirements;
      else
        this.requirements = [];
    });

    //get all statuses from the server
    this.statusService.loadStatuses().subscribe(data => {
      if(data.success)
        this.statuses = data.status;
      else
        this.statuses = [];
    });

    //get all user data from the database
    this.userService.getAllUsers().subscribe(data => {
      if(data.success)
        this.users = data.users;
      else
        this.users = [];
    });

    //get all lead data from the sever
    this.leadService.loadLeads().subscribe(data => {
      if(data.success)
        this.leads = data.leads;
      else
        this.leads = [];

      //add the overdue Status field based on the next followup date
      for(var i = 0; i < this.leads.length; i++){
        var date = new Date(Date.parse(this.leads[i].nextDate));
        var today = new Date();
        if(date < today)
          this.leads[i].overdueStatus = "overdue";
        else {
          today.setDate(today.getDate()+90);
          if(date < today)
            this.leads[i].overdueStatus = "near_future";
          else
            this.leads[i].overdueStatus = "safe";
        }
      }
    });
  }

  //to open followup management modal
  onLogClick(lead): void {

    //set clicked lead as modal lead
    this.modalLead = lead;

    //get all follow up details for that lead
    this.followUpService.getFollowUps(lead._id).subscribe(data => {

      if(data.success){
        this.followUps = data.followups;

        //sort followups by created
        if(this.followUps.length > 0)
          this.followUps.sort(function(prev, next) {
            return prev.created - next.created;
          });

        this.modalFollowUp = this.followUps[this.followUps.length-1];
      }
    });
  }

  //when clicked on the date in right side of modal
  onChangeFollowUp(followup): void{
    //change the modal followup to that followup to display its details
    this.modalFollowUp = followup;
  }

  //to export data of leads
  onExportLeadClick(): void {
    var leadFilter = new LeadfilterPipe();
    var filteredLeads = [];

    //apply filters to the leads array
    leadFilter.transform(this.leads, this.filter).forEach(function(lead, index) {

      //format each lead for our purpose
      var filteredLead = {
        created: new DatePipe('en-IN').transform(lead.created, 'dd-MM-yyyy'),
        name: lead.fName+ " "+ lead.lName,
        mobile: lead.mobile,
        address: lead.address,
        source: lead.source.toUpperCase(),
        requirement: lead.requirement,
        status: lead.status,
        assignment: lead.assignment,
        nextDate: new DatePipe('en-IN').transform(lead.nextDate, 'dd-MM-yyyy')
      }
      filteredLeads.push(filteredLead);
    });

    //Angular2Csv export option
    var exportOptions = {
      showLabels: true,
      useBom: true
    }

    var fileName = 'Leads_Report_'+(new DatePipe('en-IN').transform(new Date().toString(), 'dd-MM-yyyy HH:mm:ss'));

    new Angular2Csv(filteredLeads, fileName, exportOptions);
  }


  //to export details of all followUps of the lead
  onExportLogClick(lead): void {
    var expFollowUps = [];
    this.followUps.forEach(function(followup, index) {

      //format all followups
      var newFollowUp = {
        created: new DatePipe('en-IN').transform(followup.created, 'dd-MM-yyyy'),
        lead_name: lead.fName+" "+lead.lName,
        mobile: lead.mobile,
        address: lead.address,
        assigned: followup.assignment,
        next_date: new DatePipe('en-IN').transform(followup.date, 'dd-MM-yyyy'),
        source: lead.source.toUpperCase(),
        status: followup.status.toUpperCase(),
        requirement: lead.requirement.toUpperCase(),
        details: followup.detail
      }
      expFollowUps.push(newFollowUp);
    });

    //add property as labels
    var exportOptions = {
      showLabels: true,
      useBom: true
    }

    var fileName = 'Lead_FollowUps_Report_'+(new DatePipe('en-IN').transform(new Date().toString(), 'dd-MM-yyyy HH:mm:ss'));

    //export followups using Angular2Csv
    new Angular2Csv(expFollowUps, fileName, exportOptions);
  }

  //to sort leads based on created field
  onCreatedSortClick(): void {
      this.sortUtil(['created'], 'date');
  }

  //to sort leads by name of the lead
  onNameSortClick(): void {
    this.sortUtil(['fName', 'lName'], 'string');
  }

  //when source label is clicked to sort
  onSourceSortClick(): void {
    this.sortUtil(['source'], 'string');
  }

  //to sort leads by requirements
  onReqSortClick(): void {
    this.sortUtil(['requirement'], 'string');
  }

  //when clicked on status label in table
  onStatusSortClick(): void {
    this.sortUtil(['status'], 'string');
  }

  //to sort leads based on assigned user's name
  onAssignmentSortClick(): void {
    this.sortUtil(['assignment'], 'string');
  }

  //to sort base on follow up date
  onFollowupSortClick(): void {
    this.sortUtil(['nextDate'], 'date');
  }

  //utility function to set filter for orderby pipe
  //when clicked again change the order of sorting
  sortUtil(fields: any[], type): void {
      var applied = false;
      for(var i = 0; i < fields.length; i++){
        if(this.sortFilter.fields.indexOf(fields[i]) < 0){
          this.sortFilter.type = type;
          this.sortFilter.order = true;
          this.sortFilter.fields = fields;
          applied = true;
        }
      }
      if(!applied)
        this.sortFilter.order = !this.sortFilter.order;
  }
}
