//component to change password of logged in user
import { Component } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { OnlineService } from '../../services/online.service';
import { FocusAnimation, MoveAnimation } from '../../../../animation/my-animations';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: [
      '../../dashboard-master-common.css',
    './change-password.component.css'
 ],
 animations:[
		 FocusAnimation,
		 MoveAnimation
	  ]
})

export class ChangePasswordComponent {
  //data bound with html template
  state: string = 'active';
  passwordObj = {
    password: "",
    newPassword: "",
    username: "",
    confirmPassword: ""
  };

  constructor(
    private userService: UserService,
    private flashMessage: FlashMessagesService,
    private router: Router,
    private onlineService: OnlineService
  ) { }

  //when user submits the form to change password
  onSubmit(form: NgForm): void {
    //load user data from the localstorage
    var user = this.userService.loadUserData();

    //create object for update
    var object = {
      username: user.username,
      password: this.passwordObj.password,
      newPassword: this.passwordObj.newPassword
    };

    //use change password method of user service to update password
    this.userService.changePassword(object).subscribe(data => {
      if(data.success) {
        this.flashMessage.show(data.msg+' Please login again.', {cssClass: 'alert-success', timeout: 1500});

        //disconnect with socket ang go offline
        this.onlineService.getSocket().disconnect();

        //if password updated successfully then redirect to login page
        this.userService.clearStorage();
        this.router.navigate(['/login']);
      }else{

        //show error message and reset form
        this.flashMessage.show(data.msg, {cssClass: 'alert-danger', timeout: 1500});

        this.passwordObj.password = "";
        this.passwordObj.newPassword = "";
        this.passwordObj.confirmPassword = "";

        //remove focus from allinput fields
        var elements = document.getElementsByTagName('input');
        for(var i = 0; i < elements.length; i++)
          elements.item(i).blur();
        form.reset();
      }
    });
  }

}
