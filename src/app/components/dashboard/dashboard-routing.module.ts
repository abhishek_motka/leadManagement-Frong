//Routing module for root components
//routing module for the child router applet
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { StatusComponent } from './components/master/status/status.component';
import { SourceComponent } from './components/master/source/source.component';
import { RequirementComponent } from './components/master/requirement/requirement.component';
import { RoleComponent } from './components/master/role/role.component';
import { UserMngmntComponent } from './components/user-mngmnt/user-mngmnt.component';
import { LeadComponent } from './components/lead/lead.component';
import { ReportComponent } from './components/report/report.component';
import { ConsoleComponent } from './components/console/console.component';
import { AuthGuard } from '../../guards/auth-guard.service';
import { AdminGuard } from '../../guards/admin-guard.service';

//Routes that are used in root components
//these routes are only accessible by logged in user
const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        children: [
          {
            path: 'changepassword',
            component: ChangePasswordComponent
          },
          {
            path: '',
            component: ConsoleComponent
          },
          {
            path: 'master/status',
            canActivate: [AdminGuard],
            component: StatusComponent
          },
          {
            path: 'master/source',
            canActivate: [AdminGuard],
            component: SourceComponent
          },
          {
            path: 'master/requirement',
            canActivate: [AdminGuard],
            component: RequirementComponent
          },
          {
            path: 'master/role',
            canActivate: [AdminGuard],
            component: RoleComponent
          },
          {
            path: 'user',
            canActivate: [AdminGuard],
            component: UserMngmntComponent,
            data: {showAddForm: false}
          },
          {
            path: 'user/add',
            canActivate: [AdminGuard],
            component: UserMngmntComponent,
            data: {showAddForm: true, updateUser: false}
          },
          {
            path: 'user/edit/:username',
            canActivate: [AdminGuard],
            component: UserMngmntComponent,
            data: {showAddForm: true, updateUser: true}
          },
          {
            path: 'lead',
            component: LeadComponent
          },
          {
            path: 'lead/add',
            component: LeadComponent,
            data: {showAddForm: true, updateLead: false}
          },
          {
            path: 'lead/edit/:id',
            component: LeadComponent,
            data: {showAddForm: true, updateLead: true}
          },
          {
            path: 'report',
            component: ReportComponent
          }
        ]
      }
    ]
  },
];

//Export router module so that other modules can use the directives offered by RoutingModule
@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})

export class DashboardRoutingModule {}
