//this service provides function to maintain online user stat and get data of online users
import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';
import { UserService } from '../../../services/user.service';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Config } from '../../../app.config';

@Injectable()
export class OnlineService {

  server:string = Config.BACKEND_SERVER;

  constructor(
    private socket: Socket,
    private userService: UserService,
    private http: Http
  ) { }

  //send connect 'hi' message to the socket server
  sendConnectMessage(){
    var user = this.userService.loadUserData();
    var data = {username: user.username, email: user.email, name: user.name};
    this.socket.emit('hi', JSON.stringify(data));
  }

  //return the socket to be accessed in other services and components
  getSocket(): Socket {
    return this.socket;
  }

  //retrieve details of online users from the database
  getOnlineUsers(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.get(this.server+'online/get', {headers: headers}).map((res: Response) => res.json());
  }
}
