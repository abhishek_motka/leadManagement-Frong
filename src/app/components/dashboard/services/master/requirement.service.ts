//service to manage requirement option of master management
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../../../services/user.service';
import { Config } from '../../../../app.config';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()

export class RequirementService {

	private server: string = Config.BACKEND_SERVER;

	constructor(
    private http: Http,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

	//load details of all requirements
	loadRequirements(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.get(this.server+'requirements/getAll', {headers: headers}).map((res: Response) => res.json());
  }

	//add new requirement to the server
	addRequirement(newRequirement): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'requirements/add', JSON.stringify(newRequirement), {headers: headers}).map((res: Response) => res.json());
  }

	//update the details of the requirement
	//(never used, just for future use)
	updateRequirement(newRequirement): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'requirements/update', JSON.stringify(newRequirement), {headers: headers}).map((res: Response) => res.json());
  }

	//delete specific requirement data from the server
  deleteRequirement(requirement): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'requirements/delete', {headers: headers, body: JSON.stringify(requirement)}).map((res: Response) => res.json());
  }

	//function to delete all requirements from the server
	resetRequirements(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'requirements/reset', {headers: headers}).map((res: Response) => res.json());
  }
}
