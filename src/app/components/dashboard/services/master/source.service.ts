//function to manage source details in database
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../../../services/user.service';
import { Config } from '../../../../app.config';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()

export class SourceService {

	private server: string = Config.BACKEND_SERVER;

	constructor(
    private http: Http,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

	//load details of all sources stored in database
	loadSources(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.get(this.server+'sources/getAll', {headers: headers}).map((res: Response) => res.json());
  }

	//add new source to the database
	addSource(newSource): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'sources/add', JSON.stringify(newSource), {headers: headers}).map((res: Response) => res.json());
  }

	//update the details of the source
	//(never used, just for future use)
	updateSource(newSource): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'sources/update', JSON.stringify(newSource), {headers: headers}).map((res: Response) => res.json());
  }

	//to delete the source from the server
  deleteSource(source): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'sources/delete', {headers: headers, body: JSON.stringify(source)}).map((res: Response) => res.json());
  }

	//to delete details of all sources stored in database
	resetSources(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'sources/reset', {headers: headers}).map((res: Response) => res.json());
  }
}
