//service to manage master statuses data on the server
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../../../services/user.service';
import { Config } from '../../../../app.config';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()

export class StatusService {

	private server: string = Config.BACKEND_SERVER;

	constructor(
    private http: Http,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

	//retriev details of all statuses
	loadStatuses(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.get(this.server+'status/getAll', {headers: headers}).map((res: Response) => res.json());
  }

	//add new status to the server
	addStatus(newStatust): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'status/add', JSON.stringify(newStatust), {headers: headers}).map((res: Response) => res.json());
  }

	//delete status from the server
  deleteStatus(status): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'status/delete', {headers: headers, body: JSON.stringify(status)}).map((res: Response) => res.json());
  }

	//update details of the status
  updateStatus(newStatus): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'status/update', JSON.stringify(newStatus), {headers: headers}).map((res: Response) => res.json());
  }

	//remove all statuses stored in database
	resetStatus(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'status/reset', {headers: headers}).map((res: Response) => res.json());
  }
}
