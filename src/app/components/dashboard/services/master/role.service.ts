//service to handle role management
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../../../services/user.service';
import { Config } from '../../../../app.config';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()

export class RoleService {

	private server: string = Config.BACKEND_SERVER;

	constructor(
    private http: Http,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

	//retrieve details of all roles from the database
  loadRoles(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.get(this.server+'roles/getAll', {headers: headers}).map((res: Response) => res.json());
  }

	//add new role to the database
	addRole(newRole): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'roles/add', JSON.stringify(newRole), {headers: headers}).map((res: Response) => res.json());
  }

	//update the details of the role
	//(never used, just for future use)
	updateRole(newRole): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'roles/update', JSON.stringify(newRole), {headers: headers}).map((res: Response) => res.json());
  }

	//delete perticular role from the server
  deleteRole(role): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'roles/delete', {headers: headers, body: JSON.stringify(role)}).map((res: Response) => res.json());
  }

	//delete all roles from the database
	resetRoles(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'roles/reset', {headers: headers}).map((res: Response) => res.json());
  }
}
