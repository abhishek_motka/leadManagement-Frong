//Service to manage lead data on the server
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../../services/user.service';
import { Config } from '../../../app.config';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()

export class LeadService {

	private server: string = Config.BACKEND_SERVER;

	constructor(
    private http: Http,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

	//retriev all leads
	//returns all leads if user is admin
	//returns user specific all leads otherwise
  loadLeads(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.get(this.server+'leads/getAll', {headers: headers}).map((res: Response) => res.json());
  }

	//get detail of lead by lead id
	getLead(id): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    let params: URLSearchParams = new URLSearchParams();
		params.set('id', id);

		return this.http.get(this.server + 'leads/get', {headers: headers, search: params}).map((res: Response) => res.json());
  }

	//add new lead to the database
	addLead(newLead): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'leads/add', JSON.stringify(newLead), {headers: headers}).map((res: Response) => res.json());
  }

	//delete lead data from the server by lead id
	deleteLead(lead): Observable<any> {
    lead = {id: lead};
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'leads/delete', {headers: headers, body: JSON.stringify(lead)}).map((res: Response) => res.json());
  }

	//update the lead details on the server
	updateLead(newLead): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'leads/update', JSON.stringify(newLead), {headers: headers}).map((res: Response) => res.json());
  }

	//delete all leads details
	resetLeads(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'leads/reset', {headers: headers}).map((res: Response) => res.json());
  }
}
