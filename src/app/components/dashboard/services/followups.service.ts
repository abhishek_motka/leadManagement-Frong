//service to manage follow ups of leads
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserService } from '../../../services/user.service';
import { Config } from '../../../app.config';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()

export class FollowupsService {

	private server: string = Config.BACKEND_SERVER;

	constructor(
    private http: Http,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private userService: UserService
  ) { }

	//get all followups details for the given lead id
  getFollowUps(id): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    let params: URLSearchParams = new URLSearchParams();
		params.set('id', id);

		return this.http.get(this.server + 'followups/getByLead', {headers: headers, search: params}).map((res: Response) => res.json());
  }

	//add new followup for the lead
	addFollowUp(newFollowUp): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'followups/add', JSON.stringify(newFollowUp), {headers: headers}).map((res: Response) => res.json());
  }

	//delete follow up by followup id
	//(never used, just for future use)
	deleteFollowUp(id): Observable<any> {
    var followUp = {id: id};
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'followups/delete', {headers: headers, body: JSON.stringify(followUp)}).map((res: Response) => res.json());
  }

	//update the details of the followup
	//(never used, just for future use)
  updateFollowUp(newFollowUp): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.post(this.server+'followups/update', JSON.stringify(newFollowUp), {headers: headers}).map((res: Response) => res.json());
  }

	//delete all follow ups for the lead with given id
  resetFollowUps(id): Observable<any> {
    var followUp = {id: id};
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'followups/reset', {headers: headers, body: JSON.stringify(followUp)}).map((res: Response) => res.json());
  }

	//delete all follow ups
  removeFollowUps(): Observable<any> {
    this.userService.validateToken();

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.userService.loadToken());

    return this.http.delete(this.server+'followups/remove', {headers: headers}).map((res: Response) => res.json());
  }
}
