import { TestBed, inject } from '@angular/core/testing';

import { FollowupsService } from './followups.service';

describe('FollowupsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FollowupsService]
    });
  });

  it('should be created', inject([FollowupsService], (service: FollowupsService) => {
    expect(service).toBeTruthy();
  }));
});
