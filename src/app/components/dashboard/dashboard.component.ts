//main component of dashboard
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../services/user.service';
import { OnlineService } from './services/online.service';
import { Router } from '@angular/router';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit, OnDestroy {

	user: any = {};
	collapseNavBar: boolean = false;

  constructor(
		private userService: UserService,
    private onlineService: OnlineService,
    private router: Router
	) {}

  //when created use online service to connect with socket server
  ngOnInit() {
	   this.user = this.userService.loadUserData();
	   this.onlineService.getSocket().disconnect();
	   this.onlineService.getSocket().connect();
     this.onlineService.sendConnectMessage();

     //check for change in user data
     var interval_id = setInterval(() => {

       //if there is no user then redirect to login
       if(!this.user){
         clearInterval(interval_id);
         this.onlineService.getSocket().disconnect();
         this.router.navigate(['/login']);
       }else{
         try{
           //if user is changed then reconnect the socket for new user and reset the user
           var newUser = this.userService.loadUserData();
           if(newUser.username !== this.user.username){
             this.user = newUser;
             this.onlineService.getSocket().disconnect();
             this.onlineService.getSocket().connect();
             this.onlineService.sendConnectMessage();
           }
         }catch(err){
           clearInterval(interval_id);
           //if any error then disconnect the socket and redirect to login
           this.onlineService.getSocket().disconnect();
           this.router.navigate(['/login']);
         }
       }
     }, 3500);
  }

  ngOnDestroy(): void {
      this.onlineService.getSocket().disconnect();
  }

  //functions to handle collapse and expand of the navbar
  handleEvent(value): void{
		this.collapseNavBar = !this.collapseNavBar;
	}

	handleExpand(value) {
		if(this.collapseNavBar)
			this.collapseNavBar = !this.collapseNavBar;
	}
}
