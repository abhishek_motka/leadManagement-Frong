//Service to handle forgot password RequestOptions
//This service mainly works with http requests
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Config } from '../app.config';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()

export class PasswordResetService {

	//REST api server address
	private server: string = Config.BACKEND_SERVER;

	constructor(
		private http: Http,
		private router: Router,
		private flashMessage: FlashMessagesService
	) { }

	//Function to send email using the backend rest api
  sendResetEmail(email): Observable<any> {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(this.server + 'forgetpass/sendEmail', JSON.stringify(email), {headers: headers}).map((res: Response) => res.json());
  }

	//Function to verify the link clicked by user in the email
	verifyEmailLink(hash): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.server + 'forgetpass/sendEmail', JSON.stringify(hash), {headers: headers}).map((res: Response) => res.json());
  }

	//If link is verified then use this function to reset the password
  resetPassword(resetObject): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.server + 'forgetpass/resetPassword', JSON.stringify(resetObject), {headers: headers}).map((res: Response) => res.json());
  }
}
