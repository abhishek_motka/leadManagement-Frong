//This service handles all the user related requests like authentication, registration and user management
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { JwtHelper } from 'angular2-jwt';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Config } from '../app.config';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()

export class UserService {
	user = {};
	authToken: string = "";

	//To verify the validity of the token
	jwtHelper: JwtHelper = new JwtHelper();

	private server: string = Config.BACKEND_SERVER;

	constructor(
		private http: Http,
		private router: Router,
		private flashMessage: FlashMessagesService
	) { }

	//function to authenticate user
	authenticate(userCredentials: any) :Observable<any> {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		return this.http.post(this.server + 'users/authenticate', JSON.stringify(userCredentials), {headers: headers}).map((res: Response) => res.json());
	}

	//function to save user data to the localStorage
	saveUserData(user, token): void {
		localStorage.setItem('user',JSON.stringify(user));
		localStorage.setItem('id_token', token);
		this.user = user;
		this.authToken = token;
	}

	//to retrieve userdata from localStorage
	loadUserData() {
		return this.user = JSON.parse(localStorage.getItem('user'));
	}

	//to retrieve token from localStorage
	loadToken() {
		return this.authToken = localStorage.getItem('id_token');
	}

	//clear all data stored in the localStorage
	clearStorage(): void {
		localStorage.clear();
		this.user = "";
		this.authToken = "";
	}

	//to validate the expiration of the token
	validateToken() :void {
		this.authToken = this.loadToken();

		//If no token then user need to login so redirect to login page after clearing localstorage
		if(this.authToken === null || this.jwtHelper.isTokenExpired(this.authToken)){
			this.clearStorage();
			this.authToken = "";
			this.user = null;
			this.flashMessage.show('Your session expired. Please login again.', {cssClass: 'alert-warning', timeout: 2000});
			this.router.navigate(['/login']);
		}
	}

	//This function is used by auth-guards to validate the token
	isTokenValid(): boolean {
		var token = this.loadToken();
		if(!token)
			return false;

		if(token === null || this.jwtHelper.isTokenExpired(token)){
			this.clearStorage();
			this.authToken = "";
			this.user = null;
			return false;
		}

		return true;
	}

	//Auth guards uses this function to verify the stored token with the server
	isLoggedIn(): Observable<any> {
		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		return this.http.get(this.server + 'users/validate', {headers: headers}).map(() => true).catch((res: Response) => {
			return Observable.of(false);
		});
	}

	//Function to change password of current user
	changePassword(passwordObj) :Observable<any> {
		this.validateToken();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		return this.http.post(this.server + 'users/updatePassword', JSON.stringify(passwordObj), {headers: headers}).map((res: Response) => res.json());
	}

	//This function will log you out and will clear all localstorage data
	logout() :Observable<any> {
		this.validateToken();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		return this.http.get(this.server + 'users/logout', {headers: headers}).map((res: Response) => res.json());
	}

	// retrieve details of all users from the server
	getAllUsers(): Observable<any> {
		this.validateToken();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		return this.http.get(this.server + 'users/getAll', {headers: headers}).map((res: Response) => res.json());
	}

	//function to add new user to the server
	addUser(user): Observable<any> {
		this.validateToken();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		return this.http.post(this.server + 'users/register', JSON.stringify(user), {headers: headers}).map((res: Response) => res.json());
	}

	//function to retrieve userdetails by username
	getUserByUsername(username): Observable<any> {
		this.validateToken();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		let params: URLSearchParams = new URLSearchParams();
		params.set('username', username);

		return this.http.get(this.server + 'users/profile', {headers: headers, search: params}).map((res: Response) => res.json());
	}

	//get user details by the id of the user object in database
	getUser(id): Observable<any> {
		this.validateToken();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		let params: URLSearchParams = new URLSearchParams();
		params.set('id', id);

		return this.http.get(this.server + 'users/get', {headers: headers, search: params}).map((res: Response) => res.json());
	}

	//update details of the user
	updateUserDetails(newUser, username): Observable<any> {
		newUser.updateUsername = username;

		this.validateToken();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		return this.http.post(this.server + 'users/update', JSON.stringify(newUser), {headers: headers}).map((res: Response) => res.json());
	}

	//delete user from the database
	deleteUser(username): Observable<any> {
		var reqBody = {
			username: username
		};

		this.validateToken();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		return this.http.delete(this.server + 'users/delete', {headers: headers, body: JSON.stringify(reqBody)}).map((res: Response) => res.json());
	}

	//delete all user data except admins
	resetUserData(): Observable<any> {
		this.validateToken();

		let headers = new Headers();
		headers.append('Content-Type', 'application/json');
		headers.append('Authorization', this.authToken);

		return this.http.delete(this.server + 'users/reset', {headers: headers}).map((res: Response) => res.json());
	}
}
