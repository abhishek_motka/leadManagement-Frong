export class Config {
  static BACKEND_SERVER: string = "https://localhost:3000/api/";
  static COPYRIGHT: string = "Copyright @ 2017 Mobio Solutions. All Rights Reserved.";
  static SOCKET_SERVER_URL: string = "https://localhost:3000";
  static COMPANY_NAME_SHORT: string = 'MS';
  static CHART_COLORS: string[] = [
                        '#8F44AD',
                        '#2A80B9',
                        '#16A086',
                        '#27AE61',
                        '#F39C11',
                        '#D25400',
                        '#C1392B',
                        '#BEC3C7',
                        '#7E8C8D',
                        '#2D3E50'
                      ];
  
}
