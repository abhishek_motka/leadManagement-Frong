//filter data by status value of the object
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})

export class StatusfilterPipe implements PipeTransform{

  transform(items: any, status: boolean): any {

    if(!items || items.length <= 0)
      return items;

    if(!('status' in items[0]))
      return items;

    return items.filter(function(value) {
        if(value.status === status)
          return true;
        else
          return false;
      });
    }

}
