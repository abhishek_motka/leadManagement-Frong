//Pipe to sort array of object data by its field name
//array can be sorted 4 ways: number, date, string, boolean
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  //items: array to be sorted, filter: object specifying the conditions (type of sorting, order of sorting, fields: by which array will be sorted)
  transform(items: any[], filter: any): any {

    //if there is no item or no filter return same items
    if(!items || !filter || items.length <= 0)
      return items;

    //filter object must contain order, type and fields property
    if(!filter.type || filter.order === undefined || !filter.fields || filter.fields.length <= 0)
      return items;

    //verify that all fields are present in data of array otherwise return same items
    for(var i = 0; i < filter.fields.length; i++)
      if(!(filter.fields[i] in items[0]))
        return items;

    //determine the sorting technique by type property of filter object
    if(filter.type === "string")
      return this.stringSorter(items, filter.fields, filter.order);

    else if(filter.type === 'boolean')
      return this.booleanSorter(items, filter.fields, filter.order);

    else if(filter.type === 'date')
      return this.dateSorter(items, filter.fields, filter.order);

    else if(filter.type === 'number')
      return this.numberSorter(items, filter.fields, filter.order);

    //If other type return same array
    return items;
  }

  //all sorting helper function uses order property to sort array in ascending or descending orser
  //true: ascending
  //false: descending

  //helper function to store array by numeric fields of objects
  numberSorter(items: any[], fields: string[], order: boolean): any[] {
    return items.sort(function(prev, next) {
      var ans = 0;
      var pdata = 0;
      var ndata = 0;

      //use first field to store data, if data are equal then use second field
      for(var i = 0; i < fields.length; i++){
        pdata = parseInt(prev[fields[i]]);
        ndata = parseInt(next[fields[i]]);
        if(pdata > ndata)
          if(order)
            return 1;
          else
            return -1;
        else if(pdata < ndata)
          if(order)
            return -1;
          else
            return 1;
      }
      return 0;
    });
  }


  //function to sort array of object by date fields
  dateSorter(items: any[], fields: string[], order: boolean): any[] {
    return items.sort(function(prev, next) {

      //convert date string to date object for comparison
      var pdate = new Date(Date.parse(prev[fields[0]]));
      var ndate = new Date(Date.parse(next[fields[0]]));

      if(pdate > ndate)
        if(order)
          return 1;
        else
          return -1;
      else if(pdate < ndate)
        if(order)
          return -1;
        else
          return 1;
      else
        return 0;
    });
  }

  //function to sort array of object by boolean fields
  //true is smaller than false
  booleanSorter(items: any[], fields: string[], order: boolean): any[] {
    return items.sort(function(prev, next) {
      var pdata = true;
      var ndata = true;

      //use all fields to decide the truth value
      for(var i = 0; i < fields.length; i++){
        pdata = pdata && prev[fields[i]];
        ndata = ndata && next[fields[i]];
      }

      if(pdata === ndata)
        return 0;
      if(pdata === true && ndata === false)
        if(order)
          return -1;
        else
          return 1;
      else
        if(order)
          return 1;
        else
          return -1;
      });
  }

  //helper function to sort array of object by string fields
  //all fields will be concatenated by order to determine their order
  //sorting will be case-insensitive
  stringSorter(items: any[], fields: string[], order: boolean): any[] {

    return items.sort(function(prev, next){
      var pdata = "";
      var ndata = "";
      for(var i = 1; i < fields.length; i++){
        pdata += prev[fields[i-1]]+" ";
        ndata += next[fields[i-1]]+" ";
      }
      pdata += prev[fields[fields.length-1]];
      ndata += next[fields[fields.length-1]];

      //convert data to lowercase
      pdata = pdata.toLowerCase();
      ndata = ndata.toLowerCase();

      if(pdata > ndata)
        if(order)
          return 1;
        else
          return -1;
      else if(pdata === ndata)
        return 0;
      else
        if(order)
          return -1;
        else
          return 1;
    });
  }
}
