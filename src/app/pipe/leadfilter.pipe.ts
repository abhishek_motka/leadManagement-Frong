//Pipe to filter leads data by the filters provided in html layout
import { Pipe, PipeTransform, Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'leadfilter'
})

@Injectable()

export class LeadfilterPipe implements PipeTransform {

  //filters data matched with all filter mentioned in filter object
  transform(items: any, filter: any): any {
    if(!items || !filter)
      return items;

    //use inbuilt filter function for filtering
    return items.filter(function(lead) {
      var ans = true;

      //filter lead data by the lead created
      if(filter.created){
        var datePipe = new DatePipe('en-IN');

        //change string date to the required format by using built in DatePipe
        var leadDate = datePipe.transform(lead.created, 'dd-MM-yyyy');
        var filterDate = datePipe.transform(filter.created, 'dd-MM-yyyy');

        ans = (leadDate === filterDate) && ans;
      }

      //filter leads by the lead name
      if(filter.name) {
        var name = lead.fName + " " + lead.lName;
        if(name.toLowerCase().indexOf(filter.name.toLowerCase()) < 0)
          ans = false;
      }

      //filter leads data by mobile number of the leads
      if(filter.mobile) {
        if(lead.mobile.indexOf(filter.mobile) < 0)
          ans = false;
      }

      //filter leads by their status
      if(filter.status) {
        if(lead.status.toUpperCase() !== filter.status.toUpperCase())
          ans = false;
      }

      //filter leads by their source
      if(filter.source) {
        if(lead.source.toUpperCase() !== filter.source.toUpperCase())
          ans = false;
      }

      //filter leads by requirements
      if(filter.requirement) {
        if(lead.requirement.toUpperCase() !== filter.requirement.toUpperCase())
          ans = false;
      }

      //filter leads by the name of employee assigned
      if(filter.assignment) {
        if(lead.userid !== filter.assignment)
          ans = false;
      }

      //filter based on overdueStatus
      if(filter.ndate) {
        if(filter.ndate === 'red' && lead.overdueStatus !== 'overdue')
          ans = false;
        else if(filter.ndate === 'purple' && lead.overdueStatus !== 'near_future')
          ans = false;
        else if(filter.ndate === 'green' && lead.overdueStatus !== 'safe')
          ans = false;
      }

      return ans;
    });
  }

}
