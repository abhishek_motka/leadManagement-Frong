//Pipe to filter data master management components like status, source, role and requirement
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'masterFilter'
})
export class MasterFilterPipe implements PipeTransform {

  //master management data can be filtered by their status or their names
  transform(items: any, filter: any): any {
    if(!items || !filter)
      return items;

    //use in built filter function of array to filter master management data
    return items.filter(function(item) {
      var ans = true;
      if(filter.name) {
        if(item.name.toLowerCase().indexOf(filter.name.toLowerCase()) < 0)
          ans = false;
      }
      if(filter.status) {
        if(item.status === false && filter.status.toLowerCase() === 'open')
          ans = false;
        else if(item.status === true && filter.status.toLowerCase() === 'close')
          ans = false;
      }
      return ans;
    });
  }

}
