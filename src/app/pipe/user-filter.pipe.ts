//pipe to filter user data by different fields provided in html template

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userFilter'
})
export class UserFilterPipe implements PipeTransform {

  transform(items: any, filter: any): any {
    if(!items || !filter)
      return items;

    return items.filter(function(user) {
      var ans = true;
      if(filter.name) {
        var name = user.firstName + " " + user.lastName;
        if(name.toLowerCase().indexOf(filter.name.toLowerCase()) < 0)
          ans = false;
      }
      if(filter.type) {
        if(user.type.toLowerCase() !== filter.type.toLowerCase())
          ans = false;
      }
      if(filter.email) {
        if(user.email.toLowerCase().indexOf(filter.email.toLowerCase()) < 0)
          ans = false;
      }
      if(filter.status) {
        if(user.status === false && filter.status.toLowerCase() === 'open')
          ans = false;
        else if(user.status === true && filter.status.toLowerCase() === 'close')
          ans = false;
      }
      return ans;
    });
  }

}
