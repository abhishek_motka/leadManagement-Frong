//******************************************************//
//         						//
//	created by:					//
//		Abhishek Patel(adpatel3697@gmail.com)	//
//		Harshit Gelat(harshitgelat@gmail.com)	//
//							//
//	project: Lead Management			//
//							//
//	company: Mobio Solutions			//
//							//
//******************************************************//

This project uses currently trending MEAN stack.

This project has three folders:
data: Consists mongodb database files
leadManagement: Angular2 project
Server: Node.js and Express.js backend server

How to run the project
1) First run mongodb server on mongodb://127.0.0.1:27017

	cmd: mongod --dbpath .\data (assuming you are in project folder)

2) When mongodb server is up run node server by running the following command
	
	cmd: node .\server\app.js

3) i) Access to prebuilt project:

	https://localhost:3000 (make sure to use https)

   
   ii) (Build yourself) 
       
       Change your current directory to  (path to project folder)\leadManagement
	
	cmd: npm start

4) Open your browser and go to https://localhost:3000/api/users/logout (not required if you are usein prebuilt project)
   This is required because our server is using self signed security certificate and you need to import that certificate in your browser.
	
	open: http://localhost:4200