import { LeadManagementPage } from './app.po';

describe('lead-management App', () => {
  let page: LeadManagementPage;

  beforeEach(() => {
    page = new LeadManagementPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
